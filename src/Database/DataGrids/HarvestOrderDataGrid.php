<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Database\DataGrids;

use DB;
use SIAM618\Core\Support\DataGrids\QueryDataGrid;
use SIAM618\Agreement\Database\Models\Model;
use SIAM618\Agreement\Database\Queries\HarvestOrderQuery;
use SIAM618\Agreement\Http\Resources\HarvestOrderDataGridResource;

class HarvestOrderDataGrid extends QueryDataGrid
{

    /**
     * Data resource
     * @var
     */
    protected $resource = HarvestOrderDataGridResource::class;

    /**
     * Columns Definition
     */
    protected function columns()
    {
        // $this->column('id','p.id');
    }
    /**
     * Get the query object
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function query(HarvestOrderQuery $query)
    {
        return $query();
    }
}