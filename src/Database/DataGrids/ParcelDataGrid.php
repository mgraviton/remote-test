<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Database\DataGrids;

use DB;
use SIAM618\Core\Support\DataGrids\QueryDataGrid;
use SIAM618\Agreement\Database\Models\Model;
use SIAM618\Agreement\Database\Queries\ParcelQuery;
use SIAM618\Agreement\Http\Resources\ParcelDataGridResource;

class ParcelDataGrid extends QueryDataGrid
{

    /**
     * Data resource
     * @var
     */
    protected $resource = ParcelDataGridResource::class;

    /**
     * Columns Definition
     */
    protected function columns()
    {
        $this->column('id','p.id')
            ->column('name','p.name')
            ->column('agricultural_cycle','p.agricultural_cycle')
            ->column('code_ormva','p.code_ormva')
            ->column('annuel_surface','p.annuel_surface')
            ->column('tenure','p.tenure')
            ->column('exploited_surface','p.exploited_surface')
            ->column('manuel_surface','p.manuel_surface')
            ->column('gps_surface','p.gps_surface')
            ->column('harvested_surface','p.harvested_surface')
            ->column('abandoned_surface','p.abandoned_surface')
            ->column('cleared_surface','p.cleared_surface')
            ->column('stricken_surface','p.stricken_surface')
            ->column('irrigation_mode','p.irrigation_mode')
            ->column('logical_parcel_id','p.logical_parcel_id')
            ->column('contract_id','c.id')
            ->column('application_date','c.application_date')
            ->column('expiration_date','c.expiration_date')
            ->column('signature_date','c.signature_date')
            ->column('contract_type','c.type')
            ->column('contract_code','c.code')
            ->column('status','c.status')
            ->column('contracted_surface','c.contracted_surface')
            ->column('compaign_surface','c.compaign_surface')
            ->column('campaigns_start_date','co.start_date')
            ->column('costum_fields','c.costum_fields')
            ->column('parent_id','c.parent_id')
            ->column('full_name','tp.full_name')
            ->column('third_party_id','tp.id')
            ->column('third_party_code', 'tp.code')
            ->column('code_as400','tp.code_as400')
            ->column('third_party_type','tp.type')
            ->column('company_name','tp.company_name')
            ->column('rc','tp.rc')
            ->column('patent_number','tp.patent_number')
            ->column('if','tp.if')
            ->column('ice','tp.ice')
            ->column('cin','tp.cin')
            ->column('patent_number','tp.patent_number')
            ->column('bank_account_number','tp.bank_account_number')
            ->column('bank_code','tp.bank_code')
            ->column('full_name','tp.full_name')
            ->column('soil_id','s.id')
            ->column('registration_number','s.registration_number')
            ->column('rural_commune','s.rural_commune')
            ->column('perimeter','s.perimeter')
            ->column('region','s.region')
            ->column('district','s.district')
            ->column('cda','zc.name')
            ->column('zone','zz.name')
            ->column('sector','s.sector');
    }
    /**
     * Get the query object
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function query(ParcelQuery $query)
    {
        return $query();
    }
}