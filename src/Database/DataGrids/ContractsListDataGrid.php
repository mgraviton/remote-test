<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Database\DataGrids;

use SIAM618\Core\Support\DataGrids\QueryDataGrid;
use SIAM618\Agreement\Database\Queries\ContractsListQuery;
use SIAM618\Agreement\Http\Resources\lists\contracts\ContractsListDataGridResource;

class ContractsListDataGrid extends QueryDataGrid
{
    /**
     * Data resource.
     *
     * @var
     */
    protected $resource = ContractsListDataGridResource::class;

    /**
     * Columns Definition.
     */
    protected function columns()
    {
    }

    /**
     * Get the query object.
     *
     * @param AgreementQuery $query
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function query(ContractsListQuery $query)
    {
        return $query();
    }
}
