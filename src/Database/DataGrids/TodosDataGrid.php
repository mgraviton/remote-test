<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Database\DataGrids;

use DB;
use SIAM618\Core\Support\DataGrids\QueryDataGrid;
use SIAM618\Agreement\Database\Models\Model;
use SIAM618\Agreement\Database\Queries\TodosQuery;
use SIAM618\Agreement\Http\Resources\todos\TodosDataGridResource;

class TodosDataGrid extends QueryDataGrid
{

    /**
     * Data resource
     * @var
     */
    protected $resource = TodosDataGridResource::class;

    /**
     * Columns Definition
     */
    protected function columns()
    {
        $this
            ->column('id','t.id')
            ->column('parcel_id','p.id')
            ->column('cda_id','zc.id')
            ->column('zone_id','zz.id')
            ->column('date_debut','t.date_debut')
            ->column('date_fin','t.date_fin')
            ->column('user_id','t.user_id')
            ->column('type','t.type')
            ->column('cda_name','zc.name')
            ->column('zone_name','zc.name')
            ->column('parcelle_name','p.name')
            ->column('third_party_full_name','tp.full_name')
            ->column('third_party_cin','tp.cin')
            ->column('third_party_code','tp.code')
            ->column('advisors','ad.advisors');
    }
    /**
     * Get the query object
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function query(TodosQuery $query)
    {
        return $query();
    }
}