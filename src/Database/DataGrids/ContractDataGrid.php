<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Database\DataGrids;

use SIAM618\Core\Support\DataGrids\QueryDataGrid;
use SIAM618\Agreement\Database\Queries\ContractQuery;
use SIAM618\Agreement\Http\Resources\ContractDataGridResource;

class ContractDataGrid extends QueryDataGrid
{
    /**
     * Data resource.
     *
     * @var
     */
    protected $resource = ContractDataGridResource::class;

    /**
     * Columns Definition.
     */
    protected function columns()
    {
        $this->column('parent_id', 'a.parent_id')
            ->column('contract_type', 'a.parent_id')
            ->column('code', 'a.code')
            ->column('application_date', 'a.application_date')
            ->column('expiration_date', 'a.expiration_date')
            ->column('signature_date', 'a.signature_date')
            ->column('status', 'a.status')
            ->column('structure_name', 's.name')
            ->column('structure_code', 's.code')
            ->column('structure_type', 's.type')
            ->column('third_code', 'tp.code_as400')
            ->column('third_type', 'tp.type')
            ->column('third_company_name', 'tp.company_name')
            ->column('third_rc', 'rc')
            ->column('third_patent_number', 'tp.patent_number')
            ->column('third_if', 'tp.if')
            ->column('third_ice', 'tp.ice')
            ->column('third_cin', 'tp.cin')
            ->column('third_full_name', 'tp.full_name')
            ->column('third_full_name_ar', 'tp.full_name_ar');
    }

    /**
     * Get the query object.
     *
     * @param AgreementQuery $query
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function query(ContractQuery $query)
    {
        return $query();
    }
}
