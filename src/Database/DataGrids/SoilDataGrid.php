<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Database\DataGrids;

use DB;
use SIAM618\Core\Support\DataGrids\QueryDataGrid;
use SIAM618\Agreement\Database\Models\Soil;
use SIAM618\Agreement\Database\Queries\SoilQuery;
use SIAM618\Agreement\Http\Resources\SoilDataGridResource;

class SoilDataGrid extends QueryDataGrid
{

    /**
     * Data resource
     * @var
     */
    protected $resource = SoilDataGridResource::class;

    /**
     * Columns Definition
     */
    protected function columns()
    {
        $this->column('name')
            ->column('description');
    }

    /**
     * Get the query object
     *
     * @param AgreementQuery $query
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function query()
    {
        return DB::table('soils');
    }
}