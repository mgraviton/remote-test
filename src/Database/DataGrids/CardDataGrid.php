<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Database\DataGrids;

use DB;
use SIAM618\Core\Support\DataGrids\QueryDataGrid;
use SIAM618\Agreement\Database\Models\Model;
use SIAM618\Agreement\Database\Queries\CardQuery;
use SIAM618\Agreement\Http\Resources\CardDataGridResource;

class CardDataGrid extends QueryDataGrid
{
    /**
     * Data resource
     * @var
     */
    protected $resource = CardDataGridResource::class;

    /**
     * Columns Definition
     */
    protected function columns()
    {
        $this->column('perimeter', 'g.perimeter')
            ->column('cin', 'tp.cin')
            ->column('g.zone', 'zz.name')
            ->column('g.cda', 'zc.name')
            ->column('structure_name', 'div.name')
            ->column('rib', 'tp.bank_account_number')
            ->column('Cardstatus', 'c.status');
    }

    /**
     * Get the query object
     *
     * @param AgreementQuery $query
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function query(CardQuery $query)
    {
        return $query();
    }

    public function bulkActivate($ids)
    {
        DB::table('cards')->whereIn('id', $ids)->where('status', '!=', 'lost')->update(['status' => 'active']);
    }

    public function bulkDeactivate($ids)
    {
        DB::table('cards')->whereIn('id', $ids)->where('status', '!=', 'lost')->update(['status' => 'inactive']);
    }
}
