<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Database\DataGrids;

use DB;
use SIAM618\Core\Support\DataGrids\QueryDataGrid;
use SIAM618\Agreement\Database\Models\Model;
use SIAM618\Agreement\Database\Queries\CardGeneratorQuery;
use SIAM618\Agreement\Http\Resources\CardGeneratorDataGridResource;

class CardGeneratorDataGrid extends QueryDataGrid
{
    /**
     * Data resource
     * @var
     */
    protected $resource = CardGeneratorDataGridResource::class;

    /**
     * Columns Definition
     */
    protected function columns()
    {
        $this->column('perimeter', 'g.perimeter')
            ->column('cda', 'zc.name')
            ->column('zone', 'zz.name')
           ->column('structure', 's.name')
           ->column('structure_id', 's.id')

            
           // ->column('sector', 'g.sector')
            ->column('cin', 'tp.cin');
    }

    /**
     * Get the query object
     *
     * @param AgreementQuery $query
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder
     */
    public function query(CardGeneratorQuery $query)
    {
        return $query();
    }
}