<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by graviton developers, May 2018.
 */

namespace SIAM618\Agreement\Database\Queries;

use DB;
use SIAM618\Core\Support\Database\Query;

class CardQuery extends Query
{
    /**
     * The query definition.
     *
     * @return mixed
     */
    protected function definition()
    {

        $zones = DB::table('user_zones')->whereUserId(user_id())->pluck('zone_id')->all();

        if(user_has_role('CD') || user_has_role('CD')) {
            return DB::table('cards AS c')
                ->join('third_parties AS tp', 'c.third_party_id', '=', 'tp.id')
                ->join('contracts AS a', 'tp.id', '=', 'a.third_party_id')
                ->join('parcels AS p', 'a.id', '=', 'p.contract_id')
                ->join('soils AS s', 's.id', '=', 'p.soil_id')
                ->join('zones AS zc', 's.cda', '=', 'zc.id')
                ->join('zones AS zz', 's.zone', '=', 'zz.id')
                ->join('structures as div', 'div.id', 'c.structure_id')
                //->join('zone_advisors as za', 'za.zone_id', 'zz.id')
                ->whereIn('zz.id', $zones)
                ->whereNull('p.deleted_at')
                ->whereNull('p.id')
                ->whereNull('c.deleted_at')
                ->where('div.id', user_division_id())
                ->select(
                    DB::RAW('distinct on (c.id) c.id'),
                    'tp.*',
                    'tp.tel1 as tel1',
                    'div.name as structure_name',
                    's.perimeter',
                    's.region',
                    's.district',
                    'zc.name as cda',
                    'zz.name as zone',
                    's.sector',
                    's.block',
                    'c.status as Cardstatus',
                    'zz.name as advisers',
                    'c.id as cardId',
                    'c.rfid as rfid',
                    'c.printed_at as printed_at',
                    's.id as soilId'
                )->orderBy('c.id', 'desc');
        }

        return DB::table('cards AS c')
            ->join('third_parties AS tp', 'c.third_party_id', '=', 'tp.id')
            ->join('contracts AS a', 'tp.id', '=', 'a.third_party_id')
            ->join('parcels AS p', 'a.id', '=', 'p.contract_id')
            ->join('soils AS s', 's.id', '=', 'p.soil_id')
            ->join('zones AS zc', 's.cda', '=', 'zc.id')
            ->join('zones AS zz', 's.zone', '=', 'zz.id')
            ->join('structures as div', 'div.id', 'c.structure_id')
            //->join('zone_advisors as za', 'za.zone_id', 'zz.id')
            ->whereIn('zz.id', $zones)
            ->whereNull('p.deleted_at')
            ->whereNull('c.deleted_at')
            ->where('div.id', user_division_id())
            ->select(
                DB::RAW('distinct on (c.id) c.id'),
                'tp.*',
                'tp.tel1 as tel1',
                'div.name as structure_name',
                's.perimeter',
                's.region',
                's.district',
                'zc.name as cda',
                'zz.name as zone',
                's.sector',
                's.block',
                'c.status as Cardstatus',
                'zz.name as advisers',
                'c.id as cardId',
                'c.rfid as rfid',
                'c.printed_at as printed_at',
                's.id as soilId'
            )->orderBy('c.id', 'desc');
    }
}
