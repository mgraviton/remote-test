<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Database\Queries;

use DB;
use SIAM618\Core\Support\Database\Query;

class TodosQuery extends Query
{
    /**
     * The query definition.
     *
     * @return mixed
     */
    protected function definition()
    {
        return DB::table('todos AS t')
            ->join('parcels AS p', 'p.id', '=', 't.parcel_id')
            ->join('contracts AS c', 'p.contract_id', '=', 'c.id')
            ->join('campaigns AS co', 'c.campaign_id', '=', 'co.id')
            ->join('third_parties AS tp', 'c.third_party_id', '=', 'tp.id')
            ->join('zones AS zc', 't.cda_id', '=', 'zc.id')
            ->join('zones AS zz', 't.zone_id', '=', 'zz.id')
            ->join('zone_advisors AS ad', 'ad.zone_id', '=', 'zz.id')
            ->where('c.structure_id', user_division_id())
            ->distinct('p.id')
            ->select(
                't.id as id',
                't.parcel_id as parcel_id',
                't.cda_id as cda_id',
                't.zone_id as zone_id',
                't.date_debut as date_debut',
                't.date_fin as date_fin',
                't.user_id as user_id',
                't.type as type',
                'zc.name as cda_name',
                'zz.name as zone_name',
                'p.name as parcelle_name',
                'tp.full_name as third_party_full_name',
                'tp.cin as third_party_cin',
                'tp.code as third_party_code',
                'ad.advisors as advisors'
            );
    }
}
