<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by graviton developers, May 2018.
 */

namespace SIAM618\Agreement\Database\Queries;

use DB;
use SIAM618\Core\Support\Database\Query;

class CardGeneratorQuery extends Query
{
    /**
     * The query definition.
     *
     * @return mixed
     */
    protected function definition()
    {
      
     
       return DB::table('cards AS c')
            ->join('third_parties AS tp', 'c.third_party_id', '=', 'tp.id')
            ->join('contracts AS a', 'tp.id', '=', 'a.third_party_id')
           ->join('structures AS s', 's.id', '=', 'a.structure_id')
            ->join('parcels AS p', 'a.id', '=', 'p.contract_id')
            ->join('soils AS g', 'g.id', '=', 'p.soil_id')
            ->join('zones AS zc', 'g.cda', '=', 'zc.id')
            ->join('zones AS zz', 'g.zone', '=', 'zz.id')
            ->select(
                DB::raw('DISTINCT ON (id) tp.id'),
                'tp.*',
                'g.perimeter',
                'g.region',
                'g.district',
                's.name as structure',
                's.id as structure_id',
                's.name as name',
                's.name_ar as name_ar',
                's.address as address',
                's.tel1 as tel',
                'a.id as contract_id',
                'p.tenure',
                'zc.name as cda',
                'zz.name as zone',
                // 'g.sector',
                'g.block',
                'c.status',
                'c.id as cardId',
                'g.id as soilId'
            )
            ->whereNull('c.rfid')
            ->whereNull('c.printed_at')
            ->where('a.structure_id', user_division_id())
            ->orderBy('tp.id')
            ->orderBy('a.id', 'DESC');
    }
}