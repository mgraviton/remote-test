<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Database\Queries;

use DB;
use SIAM618\Core\Support\Database\Query;

class ParcelQuery extends Query
{
    /**
     * The query definition.
     *
     * @return mixed
     */
    protected function definition()
    {
        return DB::table('parcels AS p')
            ->join('contracts AS c', 'p.contract_id', '=', 'c.id')
            //->join('contracts AS t', 'p.contract_id', '=', 'c.id')
            ->join('campaigns AS co', 'c.campaign_id', '=', 'co.id')
            ->join('soils AS s', 'p.soil_id', '=', 's.id')
            ->join('third_parties AS tp', 'c.third_party_id', '=', 'tp.id')
            ->join('zones AS zc', 's.cda', '=', 'zc.id')
            ->join('zones AS zz', 's.zone', '=', 'zz.id')
            ->whereNull('p.deleted_at')
            ->where('p.is_logical', '=', 'true')
            ->where('c.structure_id', user_division_id())
            ->select(
                'p.id',
                'p.agricultural_cycle',
                'p.code_ormva',
                'p.name',
                'p.is_logical',
                'p.annuel_surface',
                'p.tenure',
                'p.exploited_surface',
                'p.manuel_surface',
                'p.gps_surface',
                'p.harvested_surface',
                'p.abandoned_surface',
                'p.cleared_surface',
                'p.stricken_surface',
                'p.irrigation_mode',
                'p.parcel_id',
                'c.id as contract_id',
                'c.code as contract_code',
                'c.application_date',
                'c.expiration_date',
                'c.signature_date',
                'c.type as contract_type',
                'c.status',
                'c.contracted_surface',
                'c.compaign_surface',
                'co.start_date as campaigns_start_date',
                'c.costum_fields',
                'c.parent_id',
                'tp.id as third_party_id',
                'tp.code as third_party_code',
                'tp.code_as400',
                'tp.type as third_party_type',
                'tp.company_name',
                'tp.rc',
                'tp.patent_number',
                'tp.bank_account_number',
                'tp.bank_code',
                'tp.bank_rib_key',
                'tp.if',
                'tp.code',
                'tp.ice',
                'tp.cin',
                'tp.full_name',
                's.id as soil_id',
                's.registration_number',
                's.rural_commune',
                's.perimeter',
                's.region',
                's.district',
                'zc.name as cda',
                'zz.name as zone',
                's.sector',
                's.block',
                's.total_surface',
                's.bare_surface',
                's.identified_surface',
                's.soil_number',
                's.culture_number',
                's.legal_status'
            );
    }
}
