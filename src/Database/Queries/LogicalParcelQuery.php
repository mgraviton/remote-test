<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Database\Queries;

use DB;
use SIAM618\Core\Support\Database\Query;

class LogicalParcelQuery extends Query
{
    /**
     * The query definition.
     *
     * @return mixed
     */
    protected function definition()
    {
        return DB::table('logical_parcels AS p')
            ->join('contracts AS c', 'p.contract_id', '=', 'c.id')
            ->join('campaigns AS co', 'c.campaign_id', '=', 'co.id')
            ->join('third_parties AS tp', 'c.third_party_id', '=', 'tp.id')
            ->join('soils AS s', 'p.soil_id', '=', 's.id')
            ->join('zones AS zc', 's.cda', '=', 'zc.code')
            ->join('zones AS zz', 's.zone', '=', 'zz.code')
            ->where('p.is_logical')
            ->whereNull('p.deleted_at')
            ->select(
                'p.id',
                'p.annuel_surface',
                'p.exploited_surface',
                'p.manuel_surface',
                'p.gps_surface',
                'p.harvested_surface',
                'p.abandoned_surface',
                'p.cleared_surface',
                'p.stricken_surface',
                'c.id as contract_id',
                'c.application_date',
                'c.expiration_date',
                'c.signature_date',
                'c.type as contract_type',
                'c.status',
                'c.contracted_surface',
                'c.compaign_surface',
                'co.start_date as campaigns_start_date',
                'c.costum_fields',
                'c.parent_id',
                'tp.id as third_party_id',
                'tp.code_as400',
                'tp.type as third_party_type',
                'tp.company_name',
                'tp.rc',
                'tp.patent_number',
                'tp.if',
                'tp.ice',
                'tp.cin',
                'tp.full_name',
                'zc.name as cda',
                'zz.name as zone'
            )
            ->orderByRaw('p.id');
    }
}
