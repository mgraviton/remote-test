<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Database\Queries;

use DB;
use SIAM618\Core\Support\Database\Query;

class ContractsListQuery extends Query
{
    /**
     * The query definition.
     *
     * @return mixed
     */
    protected function definition()
    {
        $zones = DB::table('user_zones as uz')->whereUserId(user_id())->pluck('zone_id')->all();

        return DB::table('contracts_list_view AS clv')
            ->whereIn('zone_id', $zones)
            ->whereStructureId(user_division_id());

    }
}
