<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Database\Queries;

use DB;
use SIAM618\Core\Support\Database\Query;

class ContractQuery extends Query
{
    /**
     * The query definition.
     *
     * @return mixed
     */
    protected function definition()
    {
        $user_id = user_id();
        $contracts = DB::table('contracts AS a')
            ->join('structures AS s', 'a.structure_id', '=', 's.id')
            ->join('third_parties AS tp', 'a.third_party_id', '=', 'tp.id');

       /* if (user_has_role('CA')) {
            $contracts = $contracts->join('parcels AS p', 'p.contract_id', '=', 'a.id')
                ->where('p.is_logical', true)
                ->whereIn('p.zone_id', function ($query) use ($user_id) {
                    $query->select('zone_id')
                        ->from('user_zones')
                        ->where('user_id', $user_id);
                });
        }*/

        $contracts = $contracts->whereNull('a.deleted_at')
            ->whereStructureId(user_division_id())
            ->select(
                'a.id as contract_id',
                'a.code',
                'a.application_date as application_date',
                'a.expiration_date as expiration_date',
                'a.signature_date as signature_date',
                'a.status as status',
                'a.parent_id as parent_contract_id',
                's.name as structure_name',
                's.code as structure_code',
                's.type as structure_type',
                'tp.code as third_party_code',
                'tp.type',
                'tp.company_name',
                'tp.rc',
                'tp.patent_number',
                'tp.if',
                'tp.ice',
                'tp.cin',
                'tp.full_name',
                'tp.full_name_ar'
            );


        return $contracts;
    }
}
