<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Database\Queries;

use DB;
use SIAM618\Base\Database\Models\Zone;
use SIAM618\Core\Support\Database\Query;
use SIAM618\Preconization\Database\Models\InterventionRequest;
use SIAM618\Preconization\Database\Models\InterventionRequestType;

class HarvestOrderQuery extends Query
{
    /**
     * The query definition.
     *
     * @return mixed
     */
    protected function definition()
    {
        $zones = DB::table('user_zones as uz')->whereUserId(user_id())->pluck('zone_id')->all();
        //$irt_types = InterventionRequestType::whereIn('costum_fields_md->global_code', ['CNVARMC', 'CNVARMA'])->pluck('id')->all();
        //$convocated = InterventionRequest::whereIn('request_type_id', $irt_types)->pluck('parcel_id')->all();
        return DB::table('harvest_order_view_mat AS hov')
            ->whereIn('z_id', $zones)
            //->whereNotIn('p_id', $convocated)
            ->where('a_structure_id', user_division_id())
            //->where('cmp_id', currentCampaign(user_division_id()))
            ->orderBy('cda_code', 'desc')
            ->orderBy('p_harvest_order', 'asc');
    }
}
