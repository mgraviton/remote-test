<?php

namespace SIAM618\Agreement\Database\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SIAM618\ThirdParty\Database\Models\ThirdParty;
use SIAM618\Base\Database\Models\Document;
use SIAM618\Base\Database\Models\Structure;
use SIAM618\Base\Database\Models\Campaign;
use Illuminate\Database\Eloquent\Builder;
use SIAM618\ThirdParty\Database\Models\RightHolder;

class Contract extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'application_date', 'code', 'expiration_date', 'signature_date', 'type', 'status','contracted_surface', 'compaign_surface', 'parent_id', 'structure_id',  'third_party_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'contracted_surface' => 'json'
        ];

    /**
     * Get the parent_ids for the agreement.

    public function agreementparent_ids()
    {
        return $this->hasMany(Agreementparent_id::class);
    }*/

    /**
     * The "booting" method of the contract.
     *
     * @return void
     */
    /*protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('structure_id', function (Builder $builder) {
            $builder->where('structure_id', '=', user_division_id());
        });
    }*/

    /**
     * Get the third party record associated with the agreement.
     */
    public function thirdParty()
    {
        return $this->belongsTo(ThirdParty::class);
    }

    /**
     * Get the campaign record associated with the agreement.
     */
    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * Get the structure for the agreement.
     */
    public function structure()
    {
        return $this->belongsTo(Structure::class);
    }

    /**
     * Get the documents for the agreement.
     */
    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    /**
     * Get the saving clause for the agreement.
     */
    public function amendments()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

     /**
     * Get all parcels for the contract.
     */
    public function parcels()
    {
        return $this->hasMany(Parcel::class);
    }


    /**
     * Get all logical parcels for the contract.
     */
    public function logical_parcels()
    {
        return $this->belongsTo(Parcel::class);
    }

    /**
     * Get all logical parcels for the contract.
     */
    public function right_holders()
    {
        return $this->hasMany(RightHolder::class);
    }
}
