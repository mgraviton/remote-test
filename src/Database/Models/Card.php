<?php

namespace SIAM618\Agreement\Database\Models;

use Illuminate\Database\Eloquent\Model;
use SIAM618\Base\Database\Models\Document;
use Illuminate\Database\Eloquent\SoftDeletes;

use SIAM618\ThirdParty\Database\Models\ThirdParty;
use SIAM618\Agreement\Database\Models\Card;
use SIAM618\Base\Database\Models\Structure;


class Card extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'rfid', 'name', 'status', 'description', 'structure_id', 'soil_id', 'third_party_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];
    
    /**
     * Get the card type record associated with the card.
     */
    public function cardType()
    {
        return $this->belongsTo(CardType::class);
    }
    
    /**
     * Get the third party record associated with the card.
     */
    public function thirdParty()
    {
        return $this->belongsTo(thirdParty::class);
    }
    
    /**
     * Get the third party record associated with the card.
     */
    public function structure()
    {
        return $this->belongsTo(Structure::class);
    }
}
