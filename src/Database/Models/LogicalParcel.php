<?php

namespace SIAM618\Agreement\Database\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SIAM618\Base\Database\Models\Campaign;
use SIAM618\ThirdParty\Database\Models\ThirdParty;

class LogicalParcel extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'annuel_surface',
        'exploited_surface',
        'manuel_surface',
        'gps_surface',
        'abandoned_surface',
        'harvested_surface',
        'cleared_surface',
        'stricken_surface',
        'campaign_id',
        'third_party_id',
        'contract_id',
        'zone_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Get the Camapign record associated with the parcel.
     */

    /**
     * Get the contract record associated with the parcel.
     */
    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

}
