<?php

namespace SIAM618\Agreement\Database\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Soil extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'registration_number', 'rural_commune', 'perimeter', 'region', 'district', 'cda', 'zone', 'sector', 'block', 'total_surface', 'bare_surface', 'identified_surface', 'soil_number', 'culture_number', 'legal_status', 'coordinate',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at',
    ];
    
    public function parcels()
    {
        return $this->hasMany(Parcel::class, 'soil_id', 'id');
    }
}
