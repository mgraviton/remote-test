<?php

namespace SIAM618\Agreement\Database\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use SIAM618\ThirdParty\Database\Models\ThirdParty;

class Parcel extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'agricultural_cycle',
        'code_ormva',
        'name',
        'is_logical',
        'annuel_surface',
        'tenure',
        'exploited_surface',
        'manuel_surface',
        'gps_surface',
        'abandoned_surface',
        'cleared_surface',
        'stricken_surface',
        'irrigation_mode',
        'soil_id',
        'contract_id',
        'parcel_id',
        'campaign_id',
        'third_party_id',
        'full_name',
        'zone_id',
        'harvested_surface',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Get the Soil record associated with the parcel.
     */
    public function soil()
    {
        return $this->belongsTo(Soil::class);
    }

    /**
     * Get the Contract record associated with the parcel.
     */
    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }


    /**
     * Get the Logical parcel record associated with the parcel.
     */
    // public function logical_parcel()
    // {
    //     return $this->belongsTo(LogicalParcel::class);
    // }

    /**
     * Get the Logical parcel record associated with the parcel.
     */
    public function logical_parcel()
    {
        return $this->belongsTo(Parcel::class, 'parcel_id');
    }
    public function physical_parcels()
    {
        return $this->hasMany(self::class);
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * Get the Third party record associated with the parcel.
     */
    public function third_party()
    {
        return $this->belongsTo(ThirdParty::class);
    }

    /**
     * Get the Parcels record associated with the parcel.
     */
    public function parcels()
    {
        return $this->hasMany(Parcel::class, 'parcel_id', 'id');
    }
}
