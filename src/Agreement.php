<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement;

use SIAM618\Contracts\AgreementContract;

class Agreement implements AgreementContract
{

    public function testExport()
    {
        return 'Exported';
    }
}