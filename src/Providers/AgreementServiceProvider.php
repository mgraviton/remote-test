<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Providers;

use \SIAM618\Core\Support\Providers\ModuleServiceProvider;

class AgreementServiceProvider extends ModuleServiceProvider
{
    /**
     * Bootstrap any module services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // Publish configuration
        $this->publishes([
            module_path('agreement', 'config/agreement.php') => config_path('agreement.php'),
        ]);

        // Load views
        $this->loadViewsFrom(module_path('agreement', 'resources/views'), 'agreement');

        // Publishes views
        $this->publishes([
            module_path('agreement', 'resources/views') => resource_path('views/vendor/agreement'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Bind the module export interface
        $this->app->bind(
            'SIAM618\Contracts\AgreementContract',
            'SIAM618\Agreement\Agreement'
        );

        // Load Configuration
        $this->mergeConfigFrom(
            module_path('agreement', 'config/agreement.php'), 'agreement'
        );
    }

}