<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Services;

use SIAM618\Base\Database\Models\Campaign;

class LogicalParcelService
{
    public function currentCampaign($id)
    {
        $campaign = Campaign::where('structure_id', $id)
            ->whereNull('end_date')->firstOrFail();

        return $campaign->id;
    }
    
}
