<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Services;

use SIAM618\Base\Database\Models\Campaign;
use SIAM618\Base\Database\Models\Zone;

class ContractService
{
    /**
     * Test Service function.
     *
     * @param $message
     *
     * @return string
     */
    public function testService($message)
    {
        return 'from service: '.$message;
    }

    public function getAttributeToPrint($contract)
    {
        $thirdParty = $contract->thirdParty;

        $data = [
            'agreement_num' => $contract->code,
            'agreement_num2' => date('Y', strtotime($contract->application_date)),
            'agreement_cin' => $thirdParty->cin,
            'agreement_full_name' => $thirdParty->full_name,
            'agreement_tel' => $thirdParty->tel1,
            'agreement_adresse' => $thirdParty->address,
            'agreement_commandement' => $thirdParty->commandment,
            'agreement_arrondissement' => $thirdParty->district,
            'agreement_rural_cummune' => $thirdParty->commune,
            'agreement_province' => enum('base.region', $thirdParty->region),
        ];
        $cmp = 1;
        if (null == $contract->parent_id && is_array($contract->contracted_surface)) {
            foreach ($contract->contracted_surface as $item) {
                $data['agreement_year_'.$cmp] = $item['campaign'];
                $data['agreement_area_'.$cmp] = $item['surface'];
                ++$cmp;
            }
            $cmp = 0;
        }
        $cmp = 1;
        $sup_total_sum = 0;
        $sup_annuel_sum = 0;
        if (null != $contract->parcels) {
            foreach ($contract->parcels as $parcel) {
                if($parcel->is_logical)
			continue;
                $soil = $parcel->soil;
                $data['agreement_cda_'.$cmp] = Zone::find($soil->cda)['name'];
                $data['agreement_zone_'.$cmp] = Zone::find($soil->zone)['name'];
                $data['agreement_code_client_'.$cmp] = $parcel->code_ormva == '' ? explode('=', $soil->registration_number)[0] : $parcel->code_ormva;
                $data['agreement_parcel_'.$cmp] = $soil->registration_number;
                $data['agreement_sup_total_'.$cmp] = $soil->total_surface;
                $data['agreement_rotation_'.$cmp] = $parcel->agricultural_cycle;
                $data['agreement_annual_sup_'.$cmp] = $parcel->annuel_surface;
                $data['agreement_rural_commune_'.$cmp] = $soil->rural_commune;
              
                $data['agreement_secteur_'.$cmp] = $soil->sector;
                $data['agreement_bloc_'.$cmp] = $soil->block;
              
                $data['agreement_mode_'.$cmp] = enum('agreement.tenure', $parcel->tenure);
                $sup_total_sum += $soil->total_surface;
                $sup_annuel_sum += $parcel->annuel_surface;
                ++$cmp;
            }
        }
        $data['agreement_sup_total_sum'] = $sup_total_sum;
        $data['agreement_annual_sup_sum'] = $sup_annuel_sum;
        $data['agreement_date'] = $contract->signature_date;

        return $data;
    }

    public function currentCampaign($id)
    {
        $campaign = Campaign::where('structure_id', $id)
            ->whereNull('end_date')->firstOrFail();

        return $campaign->id;
    }
}
