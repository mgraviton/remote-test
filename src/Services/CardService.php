<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by graviton developers, May 2018
 *
 */

namespace SIAM618\Agreement\Services;

use SIAM618\Agreement\Database\Models\Card;

class CardService
{
    /**
     * Test Service function
     * @param $message
     * @return string
     */
    function create($thirdPartyId, $structureId)
    {
        /**
         * @var Card $card
         */
        $card = Card::where('third_party_id', $thirdPartyId)
            ->where('status', '!=','lost')
            ->where('structure_id', $structureId)->get();
        /**
         *  Check for the card
         */
        if($card->count() == 0) {

            $model = new Card();

            $model->third_party_id  = $thirdPartyId;
            $model->structure_id    = $structureId;
            $model->status          = 'inactive';
            $model->save();

            return 'created';
        }


    }

    function generateCard($rfid, $type) {
        if ($type == 'agri') {
            return $this->generateArgiCard();
        }

        return null;
    }


    protected function generateAgriCard($rfid) {

        $img = Image::make(file_get_contents(module_path('agreement', 'resources/images/card_verso.png')));
        return $img->response();

    }
}