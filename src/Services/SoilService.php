<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by graviton developers, May 2018
 *
 */

namespace SIAM618\Agreement\Services;

use SIAM618\Agreement\Database\Models\Soil;

class SoilService
{
    /**
     * Test Service function
     * @param $message
     * @return string
     */
    function exist($request) 
    {
        $soil_count = Soil::where('registration_number', $request['registration_number'])
            ->where('cda', $request['cda'])
            ->where('zone', $request['zone'])->count();
        return $soil_count==0;
    }
}