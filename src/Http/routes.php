<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by graviton developers, May 2018
 */
/**
 *  Agreement routes
 */

Route::get('parcels-by-struct/{date}/{structuresList}', 'ParcelController@getParcelsByStructure');

Route::get('parcel/sync', 'IlotController@syncParcels');
Route::post('import_ilots_sync', 'IlotController@importIlotCords');

Route::post('setparcel', 'IlotController@setParcels');

Route::get('cards/export/get/{full_name}/{full_name_ar}/{code}/{name_ar}/{tel}', 'CardController@exportsGet');


Route::get('cards/download/{name}', 'CardController@download');
Route::get('parcels-by-struct/{date}/{structuresList}', 'ParcelController@getParcelsByStructure');

Route::post('cards/export', 'CardController@exports');
Route::get('cards/{id}/generate', 'CardController@generate');
Route::post('cards/generator-list', 'CardController@generatorList');
Route::put('cards/assign-rfid', 'CardController@assignRfid');

Route::post('contracts/import', 'ContractController@import');
Route::post('contracts/import-corr', 'ContractController@importCorrection');


Route::group(['middleware' => 'auth'], function () {


    Route::post('contracts/change_status', 'ContractController@change_status');

    Route::get('contracts/{contract}/prints', 'ContractController@printContract');
    Route::get('contracts/kpi', 'ContractController@kpi');
    Route::post('contracts/{id}/attach', 'ContractController@attach');
    Route::put('contracts/{contract}/activate', 'ContractController@activate');
    Route::get('contracts/vars', 'ContractController@vars');
    Route::post('contracts/list', 'ContractController@list');
    Route::api('contracts', 'ContractController');

    /**
     *  Soil routes
     */
    Route::api('soils', 'SoilController');

    /**
     *  Parcel routes
     */
    Route::put('parcels/mass', 'ParcelController@updateMass');
    Route::post('parcels/ordered', 'ParcelController@harvestOrderGrid');
    Route::post('parcels/logical', 'ParcelController@storeLogical');
    Route::api('parcels', 'ParcelController');

    /**
     *  Logical Parcel routes
     */
    // Route::post('logicalparcels/all', 'LogicalParcelController@getAllLogicalAndPhysicalParcels');
    Route::post('logicalparcels/all', 'ParcelController@getAllLogicalAndPhysicalParcels');
    // Route::get('logicalparcels/{id}', 'LogicalParcelController@getByUser');
    Route::get('logicalparcels/{id}', 'ParcelController@getLPByUser');
    Route::get('cdabyzone/{id}', 'ParcelController@getCDAByZone');
    // Route::api('logicalparcels', 'LogicalParcelController');

    /**
     * Card routes
     */

    Route::get('cards/find', 'CardController@getByRfid');
    Route::put('cards/mass-deactivate', 'CardController@massDeactivate');
    Route::put('cards/mass-activate', 'CardController@massActivate');
    Route::put('cards/{id}/deactivate', 'CardController@deactivate');
    Route::put('cards/{id}/activate', 'CardController@activate');
    Route::put('cards/{id}/cancel', 'CardController@cancel');
    Route::put('cards', 'CardController@index');
    Route::post('cards/grid', 'CardController@index');
    Route::apiResource('cards', 'CardController');
    Route::post('ilots', 'IlotController@getByZones');
    Route::post('ilot/update', 'IlotController@updateIlot');
    Route::delete('ilots/{id}', 'IlotController@deleteIlot');

    /**
     * todos api
     */
    Route::post('todos/grid', 'ParcelController@todosGrid');

});