<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Http\Controllers;

use AgreementModule;
use Illuminate\Http\Request;
use SIAM618\Core\Support\Http\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use League\Csv\Writer;
use DB;
use SIAM618\Core\Support\CSV\CsvImporter;
use SIAM618\Base\Http\Requests\UploadParcelsRequest;


class IlotController extends Controller
{

    public function getByZones(Request $request)
    {

        $zones = DB::table('user_zones')->where('user_id', user_id())->pluck('zone_id')->all();

        $geom = $request->input('geom');
        $skip = $request->input('skip');

        $ilots = DB::table('ilots AS i')
            ->join('parcels as p', 'i.parcel_id', 'p.id')
            ->join('contracts as c', 'p.contract_id', 'c.id')
            ->join('soils AS s', 'p.soil_id', '=', 's.id')
            ->join('third_parties AS tp', 'c.third_party_id', '=', 'tp.id')
            ->join('zones AS zc', 's.cda', '=', 'zc.id')
            ->join('zones AS zz', 's.zone', '=', 'zz.id')
            ->join('structure_zones as sz', 'sz.zone_id', 'zz.id')
            ->join('structures as st', 'st.id', 'sz.structure_id')
            // ->leftJoin('zone_advisors AS za', 'za.zone_id', '=', 'zz.id')
            ->leftJoin('harvest_convocations_view_mat as hcv', 'hcv.p_id', 'p.id')
            ->leftJoin('inprogress_parcels as ft', 'ft.p_id', 'p.id')
            ->whereNull('i.deleted_at')
            ->whereIn('zz.id', $zones)
            ->where('c.structure_id', user_division_id())
            ->whereNotIn('i.id', $skip)
            // ->where('c.campaign_id', currentCampaign(user_division_id()))
            ->whereRaw('st_intersects(i.polygon, st_flipcoordinates(st_geomfromgeojson( \'' . json_encode($geom) . '\')))')
            ->select(DB::RAW('
            json_build_object(
             \'type\', \'Feature\',
            \'properties\', json_build_object(
             \'p_name\', p.name,
            \'ag_name\', tp.full_name,
            \'ag_tel\', tp.tel1,
            \'date_Semis\', i.date_semis,
            \'id\', p.id,
            \'ilot_id\', i.id,
            \'cda_name\', zc.name,
            \'type\', i.type,
            \'is_cane\', c.structure_id in (8, 9),
            \'structure_id\', c.structure_id,
            \'contracted_surface\', c.contracted_surface,
            \'parcel_surface\', p.annuel_surface,
            \'ilot_surface\', round((st_area(st_transform(st_setsrid(i.polygon, 4326), 31467)) / 10000)::NUMERIC, 2),
            \'has_incident\', false,
            \'is_cane\', (st.id in (8, 9)),
            \'is_harvest_done\', false,
            \'is_harvest_inprogress\', ft.p_id,
            \'is_harvest_convocated\', hcv.p_id,
            \'is_unknown\', null,
            \'zone_name\', zz.name
             ),
             \'geometry\', st_asgeojson(ST_FlipCoordinates(i.polygon))
               ) as da
            '))
            ->get();

        //(select p_id from rotations_encodage_results where last_rotation = 'confirmed' and p_id = p.id limit 1)
        //(select p_id from rotations_encodage_results where rot_status = 'inprogress' and p_id = p.id limit 1)
        return [
            'data' => $ilots
        ];
    }

    public function syncParcels(Request $request)
    {

        //$writer = Writer::createFromString('')->setDelimiter(';');
        $user_name = $request->input('login');
        $password = $request->input('password');
        $date_syn = $request->input('date_syn');
        //$writer->insertOne([$date_syn]);
        //Storage::put('sync_parcel_'.Carbon::now()->format('YmdHs').'.csv', $writer->getContent());

        $myRequest = new \SIAM618\User\Http\Requests\LoginRequest();
        $myRequest->setMethod('POST');
        $myRequest->request->add(['email' => $user_name]);
        $myRequest->request->add(['password' => $password]);
        $userData = app('SIAM618\User\Http\Controllers\LoginController')->login($myRequest);

        if (isset($userData->email) && !property_exists($userData, 'original')) {
            $user_id = DB::table('users as u')
                ->where('u.email', $userData->email)
                ->select('u.id')
                ->get();
        } else {
            return -1;
        }
        $parcels = DB::table('zones as z')
            ->join('user_zones as uz', 'z.id', 'uz.zone_id')
            ->join('ilots as i', 'z.id', 'i.zone_id')
            ->join('parcels as p', 'p.id', 'i.parcel_id')
            ->join('zones as cda', 'cda.id', 'i.cda_id')
            ->join('campaigns as camp', 'camp.id', 'p.campaign_id')
            ->where('z.type', 'zone')
            ->where('uz.user_id', $user_id[0]->id)
            ->where('camp.structure_id', $userData->tenants[0]->division_id)
            ->select(
                'i.id as id',
                'p.name as parcelle',
                'cda.name as cda',
                'cda.id as cda_id',
                'z.name as zone',
                DB::raw("'' as semoir"),
                DB::raw("'#00FF00' as color"),
                'i.date_semis as date_semis',
                DB::raw('ST_AsGeoJSON(i.polygon) as vertices'),
                DB::raw("(CASE WHEN (i.deleted_at is null) THEN '0' ELSE '1' END) as supprimer"),
                'i.updated_at as  date_modification'
            );
        if ($date_syn != null && $date_syn != '')
            $parcels->where('i.updated_at', '>', $date_syn);

        $parcels = $parcels->get();

        foreach ($parcels as $parcel) {
            if ($parcel->vertices)
                $parcel->vertices = $this->GeoJsonToVertice($parcel->vertices);
        }
        return array(
            'code' => 200,
            'date_syn' => Carbon::now()->format('Y-m-d H:i:s'),
            'parcelles' => $parcels);
    }

    public function setParcels(Request $request)
    {

        $writer = Writer::createFromString('')->setDelimiter(';');

        $user_name = $request->input('login');
        $password = $request->input('password');

        $data = $request->input('data');
        $data = json_decode($data, true);
        if ($request->input('login') == 'r.eddamani@cosumar.co.ma') {
            $writer->insertOne([json_encode($data)]);
            Storage::put('DATA_PARCEL' . Carbon::now()->format('YmdHs') . '.csv', $writer->getContent());
        }
        $myRequest = new \SIAM618\User\Http\Requests\LoginRequest();
        $myRequest->setMethod('POST');
        $myRequest->request->add(['email' => $user_name]);
        $myRequest->request->add(['password' => $password]);
        $userData = app('SIAM618\User\Http\Controllers\LoginController')->login($myRequest);

        if (isset($userData->email) && !property_exists($userData, 'original')) {
            $user_id = DB::table('users as u')
                ->where('u.email', $userData->email)
                ->select('u.id')
                ->get();
        } else {
            return -1;
        }
        if ($data["name"] == '') {
            return response()->error(1, 'Veuillez saisir le matricule de la parcelle.');
        }
        if ($data["cda"] == '') {
            return response()->error(1, 'Veuillez saisir le CDA');
        }
        if ($data["zone"] == '') {
            return response()->error(1, 'Veuillez saisir la zone');
        }

        if ($data["surface"] == '') {
            return response()->error(1, 'Veuillez saisir la superficie');
        }

        $surface_num = explode(' ', $data["surface"])[0];
        $surface_unit = explode(' ', $data["surface"])[1];

        if ($surface_unit !== 'ha') {
            return response()->error(1, "L'unité de mesure que vous avez choisi est différentes que le Hectare.");
        }
        if ($surface_num > 200) {
            return response()->error(1, "Vous avez dépasser 200 hectares.");
        }


        if (isset($data["date_semis"]) && !empty($data["date_semis"])) {
            $semis_date = $data["date_semis"];
        } else {
            $semis_date = '';
        }


        //$userData->tenants[0]->site_code

        if (empty($data["uniqueId"]) or $data["uniqueId"] == null)
            $check_if_exists = null;
        else
            $check_if_exists = DB::table('ilots as i')->where('i.id', $data["uniqueId"])->first();

        if ($data["deleted"] == 1 && $check_if_exists) {
            $IlotID = $check_if_exists->id;
            DB::table('ilots')->where('id', $IlotID)->update(array(
                'deleted_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ));
            return array(
                "code" => "OK",
                "id" => $data["id"],
                "name" => $data["name"],
                "unique_id" => $IlotID,
                "message" => "Opération effectuée avec succès."
            );
        }

        $att_pa = DB::table('parcels as p')
            ->join('soils as s', 's.id', 'p.soil_id')
            ->join('zones as zz', 'zz.id', 's.zone')
            ->join('zones as zc', 'zc.id', 's.cda')
            ->where('p.name', $data["name"])
            ->where('zz.name', $data["zone"])
            ->where('zc.name', $data["cda"])
            ->whereNotNull('p.name')
            ->where('is_logical', true)
            ->whereNull('p.deleted_at')
            ->select('p.id as id', 'zc.id as cda_id', 'zz.id as zone_id')
            ->first();
        $coordsArray = $data["coordinates"];
        $coordsArray[count($coordsArray) + 1] = $coordsArray[0];
        $vertices = '';
        foreach ($coordsArray as $ltlng) {
            $vertices .= implode(' ', $ltlng);
            if (next($coordsArray)) {
                $vertices .= ',';
            }
        }
        $polygon = DB::Select('SELECT ST_GeomFromText(\'POLYGON((' . $vertices . '))\') as geom')[0];

        if (!$att_pa) {
            return response()->error(1, "Impossible de trouver la parcelle");
        }
        if ($check_if_exists) {
            $IlotID = $check_if_exists->id;
            DB::table('ilots')->where('id', $IlotID)->update(array(
                'date_semis' => $semis_date ? $semis_date : null,
                'parcel_id' => $att_pa->id,
                'cda_id' => $att_pa->cda_id,
                'zone_id' => $att_pa->zone_id,
                'polygon' => $polygon->geom,
                'gps_box_name' => $data["semoir"],
                'deleted_at' => $data["deleted"] == 0 ? null : Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ));
        } else {
            $IlotID = DB::table('ilots')->insertGetId(array(
                'date_semis' => $semis_date ? $semis_date : null,
                'parcel_id' => $att_pa->id,
                'cda_id' => $att_pa->cda_id,
                'zone_id' => $att_pa->zone_id,
                'polygon' => $polygon->geom,
                'gps_box_name' => $data["semoir"],
                'deleted_at' => $data["deleted"] == 0 ? null : Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ));
        }
        return array(
            "code" => "OK",
            "id" => $data["id"],
            "name" => $data["name"],
            "unique_id" => $IlotID,
            "message" => "Opération effectuée avec succès."
        );
    }

    function GeoJsonToVertice($geojson_array)
    {
        $geojson = json_decode($geojson_array, true);
        $coordinates = $geojson["coordinates"];
        $vertices = "";

        for ($i = 0; $i < (count($coordinates[0]) - 1); $i++)
            $vertices .= ($i == (count($coordinates[0]) - 2)) ?
                $coordinates[0][$i][0] . "," . $coordinates[0][$i][1]
                : $coordinates[0][$i][0] . "," . $coordinates[0][$i][1] . ",";
        return $vertices;
    }

    public function importIlotCords(Request $request, CsvImporter $importer)
    {
        if (!$request->hasFile('csv') || !$request->file('csv')->isValid()) {
            return response()->error('01541', 'Le fichier importé est invalide');
        }
        ini_set('max_execution_time', 3000);
        $importer->file($request->csv->path())
            ->rules([
                'name' => 'required',
                'cords' => 'required'
            ])
            ->onValid(function ($record) {

                $coordsArray = $record["cords"];
                $coordsArray[count($coordsArray) + 1] = $coordsArray[0];
                $vertices = '';
                dd($coordsArray);
                foreach ($coordsArray as $ltlng) {
                    $vertices .= implode(' ', $ltlng);
                    if (next($coordsArray)) {
                        $vertices .= ',';
                    }
                }
                dump('ok');
                $polygon = DB::Select('SELECT ST_GeomFromText(\'POLYGON((' . $vertices . '))\') as geom')[0];

                //$userData->tenants[0]->site_code
                dd($record["name"]);
                $att_pa = DB::table('parcels as p')
                    ->join('soils as s', 's.id', 'p.soil_id')
                    ->join('zones as zz', 'zz.id', 's.zone')
                    ->join('zones as zc', 'zc.id', 's.cda')
                    ->where('p.name', $record["name"])
                    ->whereNull('p.deleted_at')
                    ->whereNotNull('p.name')
                    ->where('is_logical', true)
                    ->select('p.id as id', 'zc.id as cda_id', 'zz.id as zone_id')
                    ->first();

                if (!$att_pa) {
                    dump($record["name"] . " : Impossible de trouver la parcelle");
                } else {
                    $IlotID = DB::table('ilots')->insertGetId(array(
                        'date_semis' => null,
                        'parcel_id' => $att_pa->id,
                        'cda_id' => $att_pa->cda_id,
                        'zone_id' => $att_pa->zone_id,
                        'polygon' => $polygon->geom,
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
                    ));
                    dump($record["name"] . " : OK");
                }


            })->onError(function ($errors, $isValidation, $record, $line) {
                dump('Error on L : ' . $line . ' => ' . $errors);
                return false;
            })
            ->import();
        if ($importer->hasErrors()) {
            return response()->error('01542', 'Le fichier importé est invalide', $importer->errors());
        }
        return response()->success('Le fichier est importé avec succès');
    }

    public function updateIlot(Request $request)
    {

        $parcel_id = $request->input('parcel_id');

        $ilot_id = $request->input('ilot_id');

        $parcel_data = DB::table('parcels as p')
            ->join('soils as s', 's.id', 'p.soil_id')
            ->join('zones as c', 'c.id', 's.cda')
            ->join('zones as z', 'z.id', 's.zone')
            ->select('z.id as zone_id', 'c.id as cda_id')
            ->first();

        $res = DB::table('ilots')
            ->where('id', $ilot_id)
            ->update([
                'parcel_id' => $parcel_id,
                'cda_id' => $parcel_data->cda_id,
                'zone_id' => $parcel_data->zone_id
            ]);

        return [
            "data" => $res
        ];
    }

    public function deleteIlot($id)
    {

        $res = DB::table('ilots as i')
            ->where('id', $id)
            ->update([
                'deleted_at' => Carbon::now()
            ]);

        return [
            "data" => $res
        ];
    }
}