<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by graviton developers, May 2018.
 */

namespace SIAM618\Agreement\Http\Controllers;

use http\Env\Response;
use Image;
use Storage;
use chillerlan\QRCode\QRCode;
use Illuminate\Http\Request;
use SIAM618\Core\Support\Http\Controller;
use SIAM618\Agreement\Database\Models\Card;
use SIAM618\Agreement\Database\DataGrids\CardDataGrid;
use SIAM618\Agreement\Database\DataGrids\CardGeneratorDataGrid;
use SIAM618\Agreement\Http\Resources\CardResource;
use SIAM618\Agreement\Http\Requests\Card\GenerateCardRequest;
use SIAM618\Agreement\Http\Requests\Card\UpdateCardRequest;
use SIAM618\Agreement\Http\Requests\Card\CardMassRequest;
use Johntaa\Arabic\I18N_Arabic;
use SIAM618\Base\Events\ActivityEvent;

use DB;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param CardDataGrid $dataGrid
     *
     * @return Response
     */
    public function index(CardDataGrid $dataGrid)
    {
        ini_set('max_execution_time', 300);
         //return DB::table('cards AS c')->get();
       /* $zones = DB::table('user_zones')->whereUserId(user_id())->pluck('zone_id')->all();

         $res =  DB::table('cards AS c')
                     ->join('third_parties AS tp', 'c.third_party_id', '=', 'tp.id')
                     ->join('contracts AS a', 'tp.id', '=', 'a.third_party_id')
                     ->join('parcels AS p', 'a.id', '=', 'p.contract_id')
                     ->join('soils AS s', 's.id', '=', 'p.soil_id')
                     ->join('zones AS zc', 's.cda', '=', 'zc.id')
                     ->join('zones AS zz', 's.zone', '=', 'zz.id')
                     ->join('structures as div', 'div.id', 'c.structure_id')

                     ->join('zone_advisors as za', 'za.zone_id', 's.zone')

                     ;
                     return [
                     "data" => $res->take(10)->get(),
                     "totalCount" => $res->count()
                     ];*/
        return $dataGrid->resource();
    }

    /**
     * Display a listing of the resource.
     *
     * @param CardDataGrid $dataGrid
     *
     * @return Response
     */
    public function generatorList(CardGeneratorDataGrid $dataGrid)
    {
        return [
            "data" => $dataGrid->resource(),
            "totalCount" => DB::table('cards AS c')
                ->join('contracts AS a', 'a.third_party_id', '=', 'c.third_party_id')
                ->whereNull('c.rfid')
                ->whereNull('c.printed_at')
                ->where('a.structure_id', user_division_id())
                ->select(DB::raw('DISTINCT ON (id) c.id'))
                ->distinct('c.id')
                ->count('c.id')
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAgreement $request
     *
     * @return Response
     */
    public function store(StoreCardRequest $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param Card $card
     *
     * @return Response
     */
    public function show(Card $card)
    {
        return response()->item($card, CardResource::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAgreement $request
     * @param int $id
     *
     * @return Response
     */
    public function update(Card $card)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Agreement $agreement
     *
     * @return Response
     */
    public function destroy(Card $card, Request $request)
    {
        $card->delete();

        event(new ActivityEvent($request, 'delete', 'carte',$card->id));


        return response()->successDelete();
    }

    /**
     * Return all needed variable.
     *
     * @return Response
     */
    public function vars()
    {
        return config('agreement');
    }

    /**
     * Assign RFID the specified resource in storage.
     *
     * @param UpdateAgreement $request
     * @param int $id
     *
     * @return Response
     */
    public function assignRfid(UpdateCardRequest $request)
    {
        foreach ($request->cards as $item) {
            $card = Card::findOrFail($item['id']);
            $card->rfid = $item['rfid'];
            $card->status = 'active';
            $card->soil_id = $item['soilId'];
            $card->printed_at = date('Y-m-d H:i:s');

            event(new ActivityEvent($request, 'active', 'carte',$card->id));

            $card->save();
        }

        return response()->success('modification a été effectuée avec succés');
    }

    /**
     * Activate the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function activate($id,Request $request)
    {
        $card = Card::where('id', $id)->where('status', 'inactive')->where('structure_id', user_division_id())->first();
        if ($card) {
            $card->status = 'active';
            $card->save();


            return response()->success('carte activer avec succès.');
        } else {
            return response()->success('aucune carte ina n\'est trouvé.');
        }
    }

    /**
     * Deactivate the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function deactivate($id , Request $request)
    {
        $card = Card::where('id', $id)->where('status', 'active')->where('structure_id', user_division_id())->first();
        if ($card) {
            $card->status = 'inactive';

            $card->save();

            event(new ActivityEvent($request, 'inactive', 'carte',$card->id));


            return response()->success('carte désactiver avec succès.');
        } else {
            return response()->success('aucune carte active n\'est trouvé.');
        }
    }

    /**
     * Deactivate the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function massDeactivate(CardMassRequest $request)
    {
        foreach ($request->cards as $item) {
            $card = Card::find($item['id']);
            $card->status = 'inactive';
            event(new ActivityEvent($request, 'inactive', 'carte',$card->id));

            $card->save();
        }

        return response()->success('cartes désactiver avec succès.');
    }

    /**
     * Deactivate the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function massActivate(CardMassRequest $request)
    {
        foreach ($request->cards as $item) {
            $card = Card::find($item['id']);
            $card->status = 'active';
            event(new ActivityEvent($request, 'active', 'carte',$card->id));

            $card->save();
        }

        return response()->success('cartes activer avec succès.');
    }

    /**
     * Cancel the specified resource from storage.
     *
     * @param Agreement $agreement
     *
     * @return Response
     */
    public function cancel(Request $request, $id)
    {
        $card = Card::where('id', $id)->whereIn('status', ['active', 'inactive'])->where('structure_id', user_division_id())->first();
        if ($card) {
            /*
             *  Update the card
             **/
            $card->status = 'lost';
            $card->description = $request->description;
            $card->save();

            /**
             *  Generate a new card.
             **/
            $newCard = new Card();
            $newCard->status = 'inproduction';
            $newCard->structure_id = $card->structure_id;
            $newCard->soil_id = $card->soil_id;
            $newCard->third_party_id = $card->third_party_id;
            $newCard->save();

            event(new ActivityEvent($request, 'inactive', 'carte',$card->id));


            return response()->success('carte désactiver avec succès.');
        } else {
            return response()->success('aucune carte n\'est trouvé.');
        }
    }

    /**
     * Export the specified resource from storage.
     *
     * @param Agreement $agreement
     *
     * @return Response
     */

    public function exports(Request $request)
    {

        $pdf = new \TCPDF('l', 'cm');


        // Set auto page breaks
        $pdf->SetAutoPageBreak(false);

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $width = $pdf->pixelsToUnits(1004);
        $height = $pdf->pixelsToUnits(650);

        $resolution = [$width, $height];

        foreach ($request->cards as $item) {
            $item['rfid'] = 'is_null';
            /* if (!isset($item['rfid'])) {
                 continue;
             }*/
            // Add recto
            $pdf->AddPage('L', $resolution);

            $path = storage_path('app/card_' . rand(0, 1000000) . '.png');

            $this->generateAgriCardRectoExport($item['rfid'], $item['full_name'], $item['full_name_ar'], $item['code'], $item['cin'], $item['structure_id'], $item['name'], $item['name_ar'], $item['tel'], $item['address'])->save($path);
            $pdf->Image($path, 0, 0, 0, 0);

            unlink($path);


            // Add Verso
            /*
            $pdf->AddPage('L', $resolution);
            $path = storage_path('app/card_'.rand(0, 1000000).'.png');
            $this->generateAgriCardVerso($item['rfid'])->save($path);
            $pdf->Image($path, 0, 0, 0, 0);

            unlink($path);
            */
        }

        $file = 'card_' . rand(0, 1000000) . '.pdf';
        $path = storage_path('app/public/' . $file);

        $pdf->Output($path, 'F');
        return ['data' => $file];


        return Storage::url($file);
    }


    public function exportsGet(Request $request)
    {


        // return  $request->full_name_ar;

        $pdf = new \TCPDF('l', 'cm');


        // Set auto page breaks
        $pdf->SetAutoPageBreak(false);

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $width = $pdf->pixelsToUnits(1004);
        $height = $pdf->pixelsToUnits(650);

        $resolution = [$width, $height];


        /* if (!isset($item['rfid'])) {
             continue;
         }*/
        // Add recto
        $pdf->AddPage('L', $resolution);

        $path = storage_path('app/card_' . rand(0, 1000000) . '.png');


        $this->generateAgriCardRectoExport('is_null', $request->full_name, $request->full_name_ar, $request->code, '', '', '', $request->name_ar, $request->tel, '')->save($path);
        $pdf->Image($path, 0, 0, 0, 0);

        unlink($path);

        // Add Verso
        /*
        $pdf->AddPage('L', $resolution);
        $path = storage_path('app/card_'.rand(0, 1000000).'.png');
        $this->generateAgriCardVerso($item['rfid'])->save($path);
        $pdf->Image($path, 0, 0, 0, 0);

        unlink($path);
        */


        $file = 'card_' . rand(0, 1000000) . '.pdf';
        $path = storage_path('app/public/' . $file);

        $pdf->Output($path, 'F');

        return response()->download(storage_path('app/public/' . $file));

    }


    public function exports1(Request $request)
    {

        $pdf = new \TCPDF('l', 'cm');

        // Set auto page breaks
        $pdf->SetAutoPageBreak(false);

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $width = $pdf->pixelsToUnits(1004);
        $height = $pdf->pixelsToUnits(650);

        $resolution = [$width, $height];

        foreach ($request->cards as $item) {
            /*   if (!isset($item['rfid'])) {
                   continue;
               }
               */

            if (!isset($item['rfid'])) {
                $item['rfid'] = 'f';
            }
            // Add recto
            $pdf->AddPage('L', $resolution);

            $path = storage_path('app/card_' . rand(0, 1000000) . '.png');
            // $this->generateAgriCardRecto($item['rfid'], $request->full_name, $item['cin'], 'SUTA')->save($path);

            $this->generateAgriCardRecto('ddd', 'rrr', 'ddd', 'dss', 'qqq', 'SUTA')->save($path);

            // $this->generateAgriCardRecto($item['rfid'],$item['full_name'],$item['full_name_ar'],$item['code'] ,$item['cin'], 'SUTA')->save($path);

            $pdf->Image($path, 0, 0, 0, 0);

            unlink($path);

            // Add Verso
            $pdf->AddPage('L', $resolution);
            $pdf->setRTL(true);

            $path = storage_path('app/card_' . rand(0, 1000000) . '.png');

            $this->generateAgriCardVerso($item['rfid'])->save($path);
            $pdf->Image($path, 0, 0, 0, 0);

            unlink($path);
        }

        $file = 'card_' . rand(0, 1000000) . '.pdf';
        $path = storage_path('app/public/' . $file);

        $pdf->Output($path, 'F');

        return ['data' => $file];


        return Storage::url($file);
    }


    public function download($name)
    {

        return response()->download(storage_path('app/public/' . $name));
    }

    /**
     * Generate the specified resource from storage.
     *
     * @param Agreement $agreement
     *
     * @return Response
     */
    public function generate(GenerateCardRequest $request, $id)
    {
        if ('verso' === $request->face) {
            return $this->generateAgriCardVerso($request->rfid)->response();
        }
        //    dd($request-cin);


        return $this->generateAgriCardRecto($request->rfid, $request->full_name, $request->full_name_ar, $request->code, $request->cin ? $request->cin : '', $request->structure, $request->name, $request->name_ar, $request->tel, $request->address)->response();
        //   return $this->generateAgriCardRecto($request->rfid, $request->full_name, $request->cin, 'SUTA')->response();
    }

    protected function generateAgriCardVerso($rfid)
    {
        $img = Image::make(file_get_contents(module_path('agreement', 'resources/images/card_verso.png')));

        $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();

        $img->rectangle(0, 350, 1004, 440, function ($draw) {
            $draw->background('#000000');
        });

        $img->insert($generator->getBarcode($rfid, $generator::TYPE_CODE_128_B, 6, 50, [255, 255, 255]), null, 150, 372);

        return $img;
    }


    protected function generateAgriCardRecto($rfid, $fullName, $fullName_ar, $code, $CIN, $structure, $name, $name_ar, $tel, $address)
    {

        $img = Image::make(file_get_contents(module_path('agreement', 'resources/images/card_recto.png')));

        $img->text(strtoupper($fullName), 72, 230, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/Optima.ttc'));
            $font->size(33);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        $Arabic = new I18N_Arabic('Glyphs');

        if ($fullName_ar == 'null') {

            $text_arabic = ' ';

        } else {
            $text_arabic = $Arabic->utf8Glyphs(' ' . $fullName_ar . ' ');

        }


        $img->text($text_arabic, 723, 160, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
            $font->size(50);
            $font->color('#000000');
            $font->align('right');
            $font->valign('top');
            $font->angle(0);
        });

        if ($code == 'null') {
            $code = ' ';
        }

        $img->text(strtoupper($code), 340, 290, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/Lato-Black.ttf'));
            $font->size(30);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        /*  $img->text(strtoupper($fullName_ar), 80, 565, function ($font) {
           $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
           $font->size(45);
           $font->color('#fdf6e3');
           $font->align('left');
           $font->valign('top');
           $font->angle(0);
       });*/


        $Arabic = new I18N_Arabic('Glyphs');

        $_text_arabic = $Arabic->utf8Glyphs(' ' . $name_ar . ' ', 500);


        $size = strlen($name_ar);


        $_text_arabic_x = null;

        if ($size < 20) {

            $_text_arabic_x = 470;
            $_text_arabic_y = 365;
        } else if ($size > 20 && $size < 50) {
            $_text_arabic_x = 550;
            $_text_arabic_y = 365;
        } else if ($size > 50 && $size < 60) {
            $_text_arabic_x = 600;
            $_text_arabic_y = 365;
        } else if ($size > 60 && $size < 93) {
            $_text_arabic_x = 670;
            $_text_arabic_y = 365;
        } else {
            $_text_arabic_x = 670;
            $_text_arabic_y = 365;
        }


        $img->text($_text_arabic, $_text_arabic_x, $_text_arabic_y, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
            $font->size(30);
            $font->color('#000000');
            $font->align('right');
            $font->valign('top');
            $font->angle(0);
        });


        $_perte_arab_txt = 'في حالة ضياع او سرقة هذه البطاقة او لتقديم شكاية ، المرجو الإتصال بالرقم الاخضر التالي';

        $_perte_arab1 = $Arabic->utf8Glyphs($_perte_arab_txt, 400);

        $img->text($_perte_arab1, 163, 445, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
            $font->size(15);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        $_perte_fr = 'En cas de perte, vol de la carte, ou pour présenter une réclamation, veuillez appeler le numéro vert suivant';

        $img->text($_perte_fr, 43, 475, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
            $font->size(14);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        $img->text($tel, 300, 500, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
            $font->size(22);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        /*
          $img->text($address, 300,510, function ($font) {
             $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
             $font->size(22);
             $font->color('#000000');
             $font->align('left');
             $font->valign('top');
             $font->angle(0);
         });


        $img->text(strtoupper($structure), 55,318, function ($font) {
             $font->file(module_path('agreement', 'resources/fonts/Lato-Regular.ttf'));
             $font->size(28);
             $font->color('#000000');
             $font->align('left');
             $font->valign('top');
             //$font->angle(90);
         });


         */

        /* $img->rectangle(800, 462, 923, 585, function ($draw) {
             $draw->background('#ffffff');
         });

         $img->insert((new QRCode())->render('qsd'), null, 790, 452);
         */

        return $img;
    }


    protected function generateAgriCardRectoExport($rfid, $fullName, $fullName_ar, $code, $CIN, $structure, $name, $name_ar, $tel, $address)
    {

        $img = Image::make(file_get_contents(module_path('agreement', 'resources/images/rec_v2.png')));

        //  $img = Image::canvas(1004, 650);

        $Arabic = new I18N_Arabic('Glyphs');
        $text_arabic = $Arabic->utf8Glyphs(' ' . $fullName_ar . ' ');

        $img->text($text_arabic, 863, 174, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
            $font->size(65);
            $font->color('#000');
            $font->align('right');
            $font->valign('top');
            $font->angle(0);
        });


        $img->text(strtoupper($fullName), 76, 260, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/Optima.ttc'));
            $font->size(50);
            $font->color('#000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        $img->text(strtoupper($code), 370, 340, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/Lato-Black.ttf'));
            $font->size(40);
            $font->color('#000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        /*  $img->text(strtoupper($fullName_ar), 80, 565, function ($font) {
           $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
           $font->size(45);
           $font->color('#fdf6e3');
           $font->align('left');
           $font->valign('top');
           $font->angle(0);
       });*/


        $Arabic = new I18N_Arabic('Glyphs');

        $_text_arabic = $Arabic->utf8Glyphs(' '. $name_ar .' ', 500);


        $size = strlen($name_ar);

        $_text_arabic_y = 396;

        $font_size = 50;

        if ($size < 20) {

            $_text_arabic_x = 580;
            $font_size = 50;

        } else if ($size > 20 && $size < 50) {
            $_text_arabic_x = 770;
            $font_size = 50;
        } else if ($size > 50 && $size < 60) {
            $_text_arabic_x = 810;
            $font_size = 50;
        } else if ($size > 60 && $size < 93) {
            $_text_arabic_x = 960;
            $font_size = 35;
        } else if ($size > 93) {
            $_text_arabic_x = 670;
            $font_size = 35;
        }


        $img->text($_text_arabic, $_text_arabic_x, $_text_arabic_y, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
            $font->size(45);
            $font->color('#000');
            $font->align('right');
            $font->valign('top');
            $font->angle(0);
        });


        $_perte_arab_txt = 'في حالة ضياع او سرقة هذه البطاقة او لتقديم شكاية ، المرجو الإتصال بالرقم المجاني التالي';

        $_perte_arab1 = $Arabic->utf8Glyphs($_perte_arab_txt, 400);

        $img->text($_perte_arab1, 10, 490, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
            $font->size(25);
            $font->color('#000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        $_perte_fr = 'En cas de perte, vol de la carte, ou pour présenter une réclamation';

        $img->text($_perte_fr, 122, 530, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/Lato-Black.ttf'));
            $font->size(21);
            $font->color('#000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        $_perte_fr = 'veuillez appeler  le numéro gratuit  suivant ';

        $img->text($_perte_fr, 150, 556, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/Lato-Black.ttf'));
            $font->size(21);
            $font->color('#000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        $img->text('&#xf095;', 567, 556, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/fontawesome-webfont.ttf'));
            $font->size(25);
            $font->color('#000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        $img->text($tel, 590, 556, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/Lato-Black.ttf'));
            $font->size(25);
            $font->color('#000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        /*      $img->text($tel, 390,485, function ($font) {
                  $font->file(module_path('agreement', 'resources/fonts/Lato-Black.ttf'));
                  $font->size(30);
                  $font->color('#000');
                  $font->align('left');
                  $font->valign('top');
                  $font->angle(0);
              });
              */


        /*
          $img->text($address, 300,510, function ($font) {
             $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
             $font->size(22);
             $font->color('#000000');
             $font->align('left');
             $font->valign('top');
             $font->angle(0);
         });


        $img->text(strtoupper($structure), 55,318, function ($font) {
             $font->file(module_path('agreement', 'resources/fonts/Lato-Regular.ttf'));
             $font->size(28);
             $font->color('#000000');
             $font->align('left');
             $font->valign('top');
             //$font->angle(90);
         });


         */

        /* $img->rectangle(800, 462, 923, 585, function ($draw) {
             $draw->background('#ffffff');
         });

         $img->insert((new QRCode())->render('qsd'), null, 790, 452);
         */

        return $img;
    }


    protected function generateAgriCardRecto2($rfid, $fullName, $fullName_ar, $code, $CIN, $structure)
    {
        // $img = Image::make(file_get_contents(module_path('agreement', 'resources/images/card_recto.png')));


        $img = Image::canvas(256, 256);


        $img->text(strtoupper($code), 80, 365, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/Lato-Black.ttf'));
            $font->size(45);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        $img->text(strtoupper($fullName), 80, 465, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/Lato-Black.ttf'));
            $font->size(45);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });


        $Arabic = new I18N_Arabic('Glyphs');
        $text_arabic = $Arabic->utf8Glyphs($fullName_ar);

        $img->text($text_arabic, 80, 405, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
            $font->size(40);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
            $font->angle(0);
        });

        /*  $img->text(strtoupper($fullName_ar), 80, 565, function ($font) {
           $font->file(module_path('agreement', 'resources/fonts/arabswell_1.ttf'));
           $font->size(45);
           $font->color('#fdf6e3');
           $font->align('left');
           $font->valign('top');
           $font->angle(0);
       });*/


        $img->text(strtoupper($structure), 980, 615, function ($font) {
            $font->file(module_path('agreement', 'resources/fonts/Lato-Regular.ttf'));
            $font->size(20);
            $font->color('#000000');
            $font->align('left');
            $font->valign('top');
            $font->angle(90);
        });

        $img->rectangle(800, 462, 923, 585, function ($draw) {
            $draw->background('#ffffff');
        });

        $img->insert((new QRCode())->render('qsd'), null, 790, 452);

        return $img;
    }

    /**
     * Display a thirdparty with the specific condition.
     *
     *
     * @return Response
     */
    public function getByRfid(Request $request)
    {
        if (!$request->has('rfid')) {
            return response()->error('01541', 'Le RFID est invalide');
        }
        $card = Card::where('rfid', $request['rfid'])->first();


        if($card == null)
            return [ 'data' => [] ];
        return response()->item($card, CardResource::class);
    }
}
