<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Http\Controllers;

use AgreementModule;
use DB;
use Illuminate\Support\Collection;
use SIAM618\Core\Support\Http\Controller;
use SIAM618\Agreement\Database\DataGrids\ContractDataGrid;
use SIAM618\Agreement\Database\DataGrids\ContractsListDataGrid;
use SIAM618\Base\Database\Models\Document;
use SIAM618\Agreement\Database\Models\Contract;
use SIAM618\Agreement\Database\Models\Soil;
use SIAM618\Agreement\Database\Models\Parcel;
use SIAM618\ThirdParty\Database\Models\ThirdParty;
use SIAM618\ThirdParty\Database\Models\ThirdPartyStructure;
use SIAM618\Agreement\Database\Models\Card;
use SIAM618\Agreement\Http\Resources\ContractResource;
use SIAM618\Agreement\Http\Resources\AgreementPdfResource;
use SIAM618\Agreement\Http\Resources\ContractDataGridResource;
use SIAM618\Agreement\Http\Requests\Contract\StoreRequest;
use SIAM618\Agreement\Http\Requests\Contract\UpdateRequest;
use SIAM618\Agreement\Http\Requests\Contract\StoreSignedContractRequest;
use SIAM618\Agreement\Http\Requests\Contract\ValidateAgreementRequest;
use SIAM618\Agreement\Http\Requests\Contract\UploadContractsRequest;
use SIAM618\Base\Http\Resources\Document\DocumentUploadResource;
use SIAM618\Core\Support\PDF\PDFFormFiller;
use SIAM618\Base\Database\Models\Template;
use SIAM618\Agreement\Services\ContractService;
use SIAM618\Agreement\Services\CardService;
use Storage;
use SIAM618\Core\Support\CSV\CsvImporter;
use League\Csv\Reader;
use League\Csv\Statement;
use Illuminate\Http\Request;
use SIAM618\User\Database\Models\User;
use SIAM618\Preconization\Database\Models\InterventionRequest;
use SIAM618\DistributionCenter\Database\Models\StockOperation;


class ContractController extends Controller
{
    use PDFFormFiller;

    /**
     * Display a listing of the resource.
     *
     * @param AgreementDataGrid $dataGrid
     * @return Response
     */
    public function grid(ContractDataGrid $dataGrid)
    {
        return $dataGrid->resource();
    }

    /**
     * Display a listing of the resource.
     *
     * @param AgreementDataGrid $dataGrid
     * @return Response
     */
    public function list(ContractsListDataGrid $dataGrid)
    {
        return $dataGrid->resource();
    }

    /**
     * Return all needed variable.
     *
     * @return Response
     */
    public function vars()
    {
        return config('agreement');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreAgreement $request
     * @return Response
     */
    public function store(StoreRequest $request, ContractService $service)
    {
        /**
         *  Add agreement
         */
        $contract = new Contract;
        $contract->structure_id = user_division_id();
        $contract->campaign_id = $service->currentCampaign($contract->structure_id);
        $contract->fill($request->all())->save();
        $contract->code = contract_code($contract);
        $contract->save();
        $third_party_structure = ThirdPartyStructure::where('third_party_id', $contract->third_party_id)
            ->where('structure_id', $contract->structure_id)->where('type', 'aggregated')->first();
        if(!$third_party_structure){
            ThirdPartyStructure::create([
                'third_party_id' 	=> $contract->third_party_id,
                'structure_id'		=> $contract->structure_id,
                'type'			=> 'aggregated'
            ]);
        }




        return response()->successStore(
            $contract,
            ContractResource::class
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Agreement $agreement
     * @return Response
     */
    public function show(Contract $contract)
    {
        return response()->item($contract, ContractResource::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateAgreement $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $agreement = Contract::findOrFail($id);
        $agreement->fill($request->except(['grounds', 'contracted_areas', 'parcels']))->save();
        return response()->successUpdate($agreement, ContractResource::class);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Agreement $agreement
     * @return Response
     */
    public function destroy(Contract $contract)
    {
        if ($contract->status === 'inprogress') {
            $parcels = $contract->parcels;
            /*foreach ($parcels as $parcel) {
                $interventionsRequest = InterventionRequest::where('parcel_id', $parcel->id)->get();
                foreach ($interventionsRequest as $interventionRequest) {
                    $stockOperation = StockOperation::where('intervention_request_id', $interventionRequest->id)->get();
                    if( count($stockOperation) > 0) {
                        return response()->error('403', 'Suppression non autorise!');
                    }
                }
            }
            foreach ($parcels as $parcel){
                $parcel->delete();
            }*/
             foreach ($parcels as $parcel) {
                 $interventionsRequest = InterventionRequest::where('parcel_id', $parcel->id)->get();
                 foreach ($interventionsRequest as $interventionRequest) {
                     $stockOperation = StockOperation::where('intervention_request_id', $interventionRequest->id)->get();
                     if( count($stockOperation) > 0) {
                         return response()->error('403', 'Suppression non autorise!');
                     }
                 }
             }
            foreach ($parcels as $parcel){
                $parcel->delete();
            }
            $contract->delete();
            return response()->successDelete();
        }



        /* old code


        if ($contract->status === 'inprogress') {
            $parcels = $contract->parcels;
            foreach ($parcels as $parcel) {
                $parcel->delete();
            }
            $contract->delete();
            return response()->successDelete();
        }
        return response()->error('403', 'Suppression non autorise!');

         */
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function attach(StoreSignedContractRequest $request, $id)
    {
        if (!$request->hasFile('document') || !$request->file('document')->isValid()) {
            return response()->error('400', 'fichier import� est invalide', 400);
        }

        /**
         * @var string $path
         * @var Object $file
         * @var Document $document
         */
        $path = '';
        $file = $request->file('document');
        $document = new Document();
        $document->user_libelle = $file->getClientOriginalName();
        $document->sys_libelle = md5($file->getClientOriginalName() . time()) . '.' . $file->extension();
        $document->exention = $file->extension();
        $document->size = $file->getClientSize();

        /**
         *  Store the template
         */
        if ($path = $file->storeAs('documents', $document->sys_libelle, 'public')) {
            $document->path = $path;
            $document->agreement_id = $id;
            $document->fill($request->all());
            $document->save();
            return response()->successStore($document, DocumentUploadResource::class);
        }

        return response()->error('400', 'une erreur est survenue lors du traitement de cette op�ration', 400);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function activate(Contract $contract, CardService $service)
    {
        /**
         * Change the agreement status
         */
        $contract->status = 'actif';
        $documents = $contract->documents()->whereIn('type', ['contract_aggregate', 'amendment'])->get();
        // $has_parent = ($contract->parent_id) ? true : false;
        // $documents = ($has_parent) ? $contract->documents()->where('type', '=', 'amendment')->get() : $contract->documents()->where('type', '=', 'contract_aggregate')->get();
        if (count($documents) > 0) {
            $contract->save();
            $service->create($contract->third_party_id, $contract->structure_id);
            return response()->success('200', 'contrat validé avec succés');
        }
        return response()->error('403', 'Importer un contrat signé avant de valider !');
    }

    /**
     *
     * @param agreement
     *
     * @return \Illuminate\Http\Response
     */
    public function printContract(Contract $contract, ContractService $service)
    {
        //$template = json_decode($contract->structure->parameters)!=null?json_decode($contract->structure->parameters)->path:'';
        //$template = $contract->structure->parameters != null ? $contract->structure->parameters->path : '';
        // return $contract;
        if ($contract->structure->parameters == null || !($template = $contract->structure->parameters['path'])) {
            return response()->error('55152', 'Merci d\'ajouter un fichier PDF');
        }

        $code = date('Y', strtotime($contract->application_date)) . '-' . sprintf('%06d', $contract->id);

        $pdfFile = $this->fillPDFForm(module_path('agreement', $template), $service->getAttributeToPrint($contract))
            ->generatePDF(false, 'CONTRAT_' . $code . '.pdf');

        $pdfFile->move(storage_path('app/public/'));

        return response()->success('Contrat géneré avec succés.', [
            'file' => url('storage/' . $pdfFile->getBasename())
        ]);
    }

    /**
     * Display the kpis.
     *
     * @param agreement
     *
     * @return \Illuminate\Http\Response
     */
    public function kpi(ContractService $service)
    {
        $count['encours'] = Contract::where('status', 'inprogress')->whereStructureId(user_division_id())->whereNull('deleted_at')->count();
        //$count['actif'] = Contract::where('status', 'actif')->whereStructureId(user_division_id())->whereNull('deleted_at')->count();
        $count['actif'] = Contract::where('status', 'actif')
            ->where('campaign_id', $service->currentCampaign(user_division_id()))
            ->whereStructureId(user_division_id())
            ->whereNull('deleted_at')->count();
        $count['inactif'] = Contract::where('status', 'inactif')->whereStructureId(user_division_id())->whereNull('deleted_at')->count();
        $count['suspendu'] = Contract::where('status', 'suspended')->whereStructureId(user_division_id())->whereNull('deleted_at')->count();
        $count['bloque'] = Contract::where('status', 'blocked')->whereStructureId(user_division_id())->whereNull('deleted_at')->count();
        $count['exprire'] = Contract::where('status', 'exprired')->whereStructureId(user_division_id())->whereNull('deleted_at')->count();

        $structre_id = user_division_id();
        $parent_ids = DB::table('contracts')->where('campaign_id', $service->currentCampaign(user_division_id()))
            ->whereNotNull('parent_id')
            ->select('parent_id')->get();

        $ids = [];
        foreach (json_decode($parent_ids) as $item) {
            $ids[] = $item->parent_id;
        }
        //$contracts = DB::table('contracts')->whereStructureId(user_division_id())->get();
        $contracts = DB::table('contracts')
            ->where('status', 'actif')
            ->whereNotIn('id', $ids)
            ->where('campaign_id', $service->currentCampaign(user_division_id()))
            ->whereStructureId(user_division_id())->get();
        $total = 0;
        foreach ($contracts as $contract) {
            /*foreach(json_decode($contract->contracted_surface) as $item){
                $total+= floatval($item->surface);
            }*/
            $total += floatval($contract->compaign_surface);
        }
        $count['sup_contracted'] = number_format($total, 2, ',', ' ');

        $count['aggregated'] = DB::table('third_parties AS t')
            ->join('contracts AS c', 't.id', '=', 'c.third_party_id')
            ->whereNull('t.deleted_at')
            ->whereStructureId(user_division_id())->count(DB::raw('DISTINCT t.id'));

        return $count;
    }

    /**
     * @param UploadArticlesRequest $request
     * @param CsvImporter $importer
     */
    public function import(UploadContractsRequest $request)
    {
        if (!$request->hasFile('csv') || !$request->file('csv')->isValid()) {
            return response()->error('01541', 'Le fichier import� est invalide');
        }
        ini_set('max_execution_time', 3000);
        $structure = [
            'M120' => 1,
            'M130' => 2,
            'M140' => 3,
            'M110' => 4,
            '2p15' => 5,
            '3p13' => 6,
            '3p22' => 7,
            '4p21' => 8,
            '4p14' => 9,
            '1p11' => 10,
            '1p12' => 11];

        $results = $this->importFromPath($request->csv->path());
        DB::beginTransaction();
        $ligne = 0;
        try {
            foreach ($results as $contract_key => $contract_item) {
                $ligne = $contract_key;
                $contract = new Contract;
                $i = true;
                foreach ($contract_item as $key2 => $contract_item_campaigns) {
                    dump('E1');
                    foreach ($contract_item_campaigns as $campaign_parcels) {
                        dump('E2');
                        if ($i) {
                            dump('E3');
                            $division_id = $structure[strtolower($campaign_parcels["z"])];
                            $contract->structure_id = $division_id;
                            dump($campaign_parcels["z"] . '  => DIVISION : ok');
                            $contract->campaign_id = $division_id == 8 ? 65 : 67;

                            dump($contract->campaign_id . '  => CAMPAIGN : ok');
                            $contract->application_date = date("Y-m-d", strtotime($campaign_parcels["u"]));
                            $str = [['campaign' => $campaign_parcels["p"], 'surface' => $campaign_parcels["q"]]];
                            $contract->contracted_surface = $str;
                            //dd($contract->contracted_surface);
                            $contract->expiration_date = date("Y-m-d", strtotime($campaign_parcels["i"]));
                            $contract->signature_date = date("Y-m-d", strtotime($campaign_parcels["y"]));
                            $contract->type = $campaign_parcels["o"] == "Pluriannuel" ? 'multiyear' : 'annual';
                            $contract->status = 'inprogress';
                            $contract->compaign_surface = floatval($campaign_parcels["q"]);
                            $thirdparty = DB::table('third_parties')
                                ->where('cin', $campaign_parcels["t"])
                                ->first();
dump($campaign_parcels["t"] . '  => TH : ok');
dump($thirdparty->id);
                            $contract->third_party_id = $thirdparty->id;
                            $contract->code = contract_code_chargement($contract->application_date, $division_id, null);

                            $contract->save();

                            $i = false;
                        }
                        else if($campaign_parcels["u"] != ''){
                            dump('E4');
                        $parent_id = $contract->id;
                        $code_parent = $contract->code;
                        $contract = new Contract;
                        $division_id = $structure[strtolower($campaign_parcels["z"])];
                        $contract->structure_id = $division_id;
// dump($campaign_parcels["z"].'  => DIVISION : ok');
                        $contract->campaign_id =  $division_id == 8 ? 65 : 67;
// dump($contract->campaign_id.'  => CAMPAIGN : ok');
                        $contract->application_date = date("Y-m-d", strtotime($campaign_parcels["u"]));
                        $contract->expiration_date = date("Y-m-d", strtotime($campaign_parcels["i"]));
                        $contract->signature_date = date("Y-m-d", strtotime($campaign_parcels["y"]));
                        $contract->type = $campaign_parcels["o"] == "Pluriannuel" ? 'multiyear' : 'annual';
                        $contract->status = 'inprogress';
                        $contract->parent_id = $parent_id;
                        $contract->compaign_surface = floatval($campaign_parcels["q"]);
                        $thirdparty = DB::table('third_parties')
                                ->where('cin',  $campaign_parcels["t"])
                                ->first();
// dump($campaign_parcels["t"].'  => TH : ok');
// dump($thirdparty->id);
                        $contract->third_party_id = $thirdparty->id;
                        $contract->code = contract_code_chargement($contract->application_date, $division_id,  $code_parent);
                        $contract->save();
                      }

dump($campaign_parcels['m']);
                        $cda = DB::table('zones as z')
                            ->join('structure_zones as sz', 'z.id', '=', 'sz.zone_id')
                            ->where('z.name', $campaign_parcels['m'])
                            ->whereType('cda')
                            ->where('structure_id', $structure[strtolower($campaign_parcels["z"])])
                            ->select('z.*')
                            ->first();
dump($cda->id);
dump($campaign_parcels['w']);
                        $zone = DB::table('zones as z')
                            ->where('name', $campaign_parcels['w'])
                            ->whereRaw('tree_path <@ ?', [$cda->tree_path])
                            ->whereRaw('nlevel(tree_path) = 3')
                            ->select('z.*')
                            ->first();
dump('zone : '.$zone->id);
                        //$soil = Soil::firstOrNew(['registration_number' => $campaign_parcels['v'], 'cda' => $campaign_parcels['m'], 'zone' => $campaign_parcels['w']]);

                        //if(!$soil->id)
                        //{
                        $soil = new Soil;

                        $soil->registration_number = $campaign_parcels['v'];
                        $soil->cda = $cda->id;
                        $soil->zone = $zone->id;
                        $soil->sector = $campaign_parcels['x'];
                        $soil->block = $campaign_parcels['c'];
                        $soil->total_surface = floatval($campaign_parcels['n']);
                        $soil->save();
                        //}
                        dump('soil ok');
dump($contract);
                        $parcel = new Parcel;
                        $parcel->zone_id = $zone->id;
                        $parcel->agricultural_cycle = $campaign_parcels['a3'];
                        $parcel->code_ormva = $campaign_parcels['b'];
                        $parcel->annuel_surface = floatval($campaign_parcels['a1']);
                        $parcel->tenure = $campaign_parcels['a2'];
                        $parcel->third_party_id = $contract->third_party_id;
                        $parcel->campaign_id = $contract->campaign_id;
                        $parcel->name = $campaign_parcels['v'];
dump($campaign_parcels['v']);
                        $parcel->irrigation_mode = $campaign_parcels['a4'];
                        $parcel->soil_id = $soil->id;
                        $parcel->contract_id = $contract->id;
                        $parcel->is_logical = true;
                        $parcel->save();

                        dump('Physiq OK');



                        $p_p_id = $parcel->id;
            dump($parcel);
                        $parcel = new Parcel;
                        $parcel->zone_id = $zone->id;
            dump('zz '.$zone->id);
                        $parcel->agricultural_cycle = $campaign_parcels['a3'];
                        $parcel->code_ormva = $campaign_parcels['b'];
                        $parcel->annuel_surface = floatval($campaign_parcels['a1']);
                        $parcel->tenure = $campaign_parcels['a2'];
                        $parcel->third_party_id = $contract->third_party_id;
            dump('tp '.$contract->third_party_id);
                        $parcel->campaign_id = $contract->campaign_id;
            dump('cm '.$contract->campaign_id);
                        $parcel->name = $campaign_parcels['v'];
                        $parcel->irrigation_mode = $campaign_parcels['a4'];
                        $parcel->soil_id = $soil->id;
            dump('so '.$soil->id);
                        $parcel->contract_id = $contract->id;
            dump('ca '.$contract->id);
                        $parcel->parcel_id = $p_p_id;
                        $parcel->save();
                        dump('logiq OK');
                    }
                    dump('cc : cc');
                }
                dump('ok : ok');
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->error('L ' . $ligne, $e->getMessage());
        }
        //DB::rollBack();

        DB::commit();
        return response()->success('Le fichier est importé avec succés');
    }

    public function currentCampaign($year, $id)
    {
        $campaign = DB::table('campaigns')->whereStructureId($id)->whereYear('start_date', '=', $year)->first();
        return $campaign->id;
    }

    public function importFromPath($str)
    {
        //Initialize the reader
        try {
            $this->csv = Reader::createFromPath($str);
            $this->csv->setDelimiter(';');
        } catch (Exception $e) {
            return $e->getMessage();
        }

        // Set the header offset and get the records
        $this->csv->setHeaderOffset(0);
        $records = iterator_to_array((new Statement())->process($this->csv));
        //dd((new Collection($records))->groupBy(["e", "p"])[10900049 ]);
        return (new Collection($records))->groupBy(["e", "p"]);

    }
    /**
     * @param UploadArticlesRequest $request
     * @param CsvImporter $importer
     */
    public function importCorrection(UploadContractsRequest $request, CsvImporter $importer)
    {
        if (!$request->hasFile('csv') || !$request->file('csv')->isValid()) {
            return response()->error('01541', 'Le fichier importé est invalide');
        }
        ini_set('max_execution_time', 3000);
        $structure = [
            'M120' => 1,
            'M130' => 2,
            'M140' => 3,
            'M110' => 4,
            '2p15' => 5,
            '3p13' => 6,
            '3p22' => 7,
            '4p21' => 8,
            '4p14' => 9,
            '1p11' => 10,
            '1p12' => 11];
        $importer->file($request->csv->path())
            ->rules([
                'id' => 'required'
            ])
            ->onValid(function ($record) use($structure) {
                dump('id -> '.$record["id"]);
              $id = $record["id"];
              $avs = Contract::where('parent_id', $id)->get();
              foreach($avs as $av){
                $id = $av->id;
                $parcels = Parcel::where('contract_id', $id)
                                    ->where('name', $record["v"])->get();
                foreach($parcels as $parcel){
                   if($parcel){
                   dump('parcel -> '.$parcel->id);
                     $cda = DB::table('zones as z')
                              ->join('structure_zones as sz', 'z.id', '=', 'sz.zone_id')
                              ->where('z.name', $record['m'])
                              ->whereType('cda')
                              ->where('structure_id', $structure[strtolower($record["z"])])
                              ->select('z.*')
                              ->first();
                     dump('cda -> '.$cda->id);
                     $zone = DB::table('zones as z')
                              ->where('name', $record['w'])
                              ->whereRaw('tree_path <@ ?', [$cda->tree_path])
                              ->whereRaw('nlevel(tree_path) = 3')
                              ->select('z.*')
                              ->first();
                     dump('zone -> '.$zone->id);
                     $soil = Soil::where('id', $parcel->soil_id)
                              ->first();
                     dump('soil -> '.$soil->id);
                     if($soil){
                       $soil->cda = $cda->id;
                       $soil->zone = $zone->id;                    
                       $soil->save();
                       dump('save soil ok');
                     }
                     $parcel->zone_id= $zone->id;
                     $parcel->save();
                     dump('save parcel ok');
                   }
                }
              }
              
            })->onError(function ($errors, $isValidation, $record, $line) {
                dump('Error on L : '.$line.' => '. $errors);
                return false;
            })
            ->import();
        if ($importer->hasErrors()) {
            return response()->error('01542', 'Le fichier importé est invalide', $importer->errors());
        }
        return response()->success('Le fichier est importé avec succès');
    }




    public function change_status(Request $request)
    {


        // $user = $this->guard()->user();

        $user =  User::find(user_id());

        if($user->rfid == $request->rfid ) {

            $contract = Contract::find( $request->id);


            if (null != $contract) {

                $contract->status = 'inprogress';
                $contract->save();

                return response()->success('200', 'status modifié avec succés');

            }

            return response()->error(404, 'Contract Introuvable !!');

        } else {
            return response()->error(405, 'Vous n\'avez pas le droit pour valider cette opération');
        }
    }

}
