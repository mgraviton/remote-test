<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Http\Controllers;

use AgreementModule;
use SIAM618\Core\Support\Http\Controller;
use SIAM618\Agreement\Database\DataGrids\ParcelDataGrid;
use SIAM618\Agreement\Database\DataGrids\LogicalParcelDataGrid;
use SIAM618\Agreement\Database\Models\Parcel;
use SIAM618\Agreement\Database\Models\Contract;
use SIAM618\Agreement\Database\Models\LogicalParcel;
use SIAM618\Agreement\Http\Resources\ParcelResource;
use SIAM618\Agreement\Http\Resources\ParcelDataGridResource;
use SIAM618\Agreement\Services\ParcelService;
use SIAM618\Agreement\Services\LogicalParcelService;
use SIAM618\Agreement\Http\Requests\Parcel\StoreRequest;
use SIAM618\Agreement\Http\Requests\Parcel\UpdateRequest;
use DB;

class LogicalParcelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param AgreementService $service
     * @param AgreementDataGrid $dataGrid
     * @return Response
     */
    public function grid(LogicalParcelDataGrid $dataGrid)
    {
        return $dataGrid->resource();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    
    public function store(StoreRequest $request, LogicalParcelService $service)
    {
        // checking if there is a parcel does not belong to the same CDA / ZONE 
        foreach ($request->items as $parcel_id)
        {
            $parcel = Parcel::find($parcel_id)->get();
            if($parcel->soil->cda != $request->cda || $parcel->soil->zone != $request->zone)
                return response()->error(1, "La parcel dont le code " . $parcel->code_ormva . "n'appartient pas à la meme CDA/Zone : ".$request->cda.'/'.$request->zone);
        }
        // here all the parcels belong to the same CDA / ZONE
        $logicalparcel = new LogicalParcel;
        $campaign_id = $service->currentCampaign(user_division_id());   // getting the current campaign
        
        //-- Filling / Saving the logical parcel record without the calculable fields ??
        $logicalparcel->fill([ $request->only(['third_party_id', 'contract_id']), 'campaign_id' => $campaign_id ])->save();
        // $logicalparcel->fill([ $request->except(['items', 'cda', 'zone']), 'campaign_id' => $campaign_id ])->save();
        
        // this array if for the calculable fields --> will be filled with the sum of physical parcels surfaces below
        $surfaces = array(
            'annuel_surface' => 0,
            'exploited_surface' => 0,
            'manuel_surface' => 0,
            'gps_surface' => 0,
            'harvested_surface' => 0,
            'abandoned_surface' => 0,
            'cleared_surface' => 0,
            'stricken_surface' => 0,
        );
        foreach ($request->items as $parcel_id) {
            $parcel = Parcel::find($parcel_id);  // Getting the physical parcel 
            $parcel->logical_parcel_id = $logicalparcel->id;    // attaching it with its logical parcel
            $surfaces = array(      // updating $surfaces array with the sum of all the physical parcels surfaces
                'annuel_surface' => $surfaces['annuel_surface'] + $parcel->annuel_surface,
                'exploited_surface' => $surfaces['exploited_surface'] + $parcel->exploited_surface,
                'manuel_surface' => $surfaces['manuel_surface'] + $parcel->manuel_surface,
                'gps_surface' => $surfaces['gps_surface'] + $parcel->gps_surface,
                'harvested_surface' => $surfaces['harvested_surface'] + $parcel->harvested_surface,
                'abandoned_surface' => $surfaces['abandoned_surface'] + $parcel->abandoned_surface,
                'cleared_surface' => $surfaces['cleared_surface'] + $parcel->cleared_surface,
                'stricken_surface' => $surfaces['stricken_surface'] + $parcel->stricken_surface,
            );
        }
        // updating the logical parcel surfaces with the array created before
        $logicalparcel->annuel_surface = $surfaces['annuel_surface'];
        $logicalparcel->exploited_surface = $surfaces['exploited_surface'];
        $logicalparcel->manuel_surface = $surfaces['manuel_surface'];
        $logicalparcel->gps_surface = $surfaces['gps_surface'];
        $logicalparcel->harvested_surface = $surfaces['harvested_surface'];
        $logicalparcel->abandoned_surface = $surfaces['abandoned_surface'];
        $logicalparcel->cleared_surface = $surfaces['cleared_surface'];
        $logicalparcel->stricken_surface = $surfaces['stricken_surface'];
        $logicalparcel->save();
        return response()->successStore($logicalparcel, LogicalParcelResource::class);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $model
     * @return Response
     */
    public function show(LogicalParcel $logicalparcel)
    {
        return response()->item($logicalparcel, LogicalParcelResource::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $model
     * @return Response
     */
    public function update(UpdateRequest $request, LogicalParcel $logicalparcel)
    {
        $parcel = Parcel::find($request['ph_id']);
        $parcel->update(['logical_parcel_id' => $logicalparcel->id]);
        
        $logicalparcel['annuel_surface'] += $parcel->annuel_surface;
        $logicalparcel['exploited_surface'] += $parcel->exploited_surface;
        $logicalparcel['manuel_surface'] += $parcel->manuel_surface;
        $logicalparcel['gps_surface'] += $parcel->gps_surface;
        $logicalparcel['harvested_surface'] += $parcel->harvested_surface;
        $logicalparcel['abandoned_surface'] += $parcel->abandoned_surface;
        $logicalparcel['cleared_surface'] += $parcel->cleared_surface;
        $logicalparcel['stricken_surface'] += $parcel->stricken_surface;
        
        $logicalparcel->fill($request->all())->save();
        return response()->successUpdate($logicalparcel, LogicalParcelResource::class);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $model
     * @return Response
     */
    public function destroy(LogicalParcel $logicalparcel)
    {
        // $logicalparcel->delete();
        // return response()->successDelete();
    }
    
    // public function getByUser($id)
    // {
    //     // $logical_parcels = LogicalParcel::whereThirdPartyId($id)->get();
    //     $logical_parcels = LogicalParcel::where('third_party_id', $id)->get();
    //     if(!$logical_parcels) return response("Not Found");
    //     return [
    //         'data' => $logical_parcels
    //     ];
    // }
    /*
    public function getAllLogicalAndPhysicalParcels()
    {
        
        // Getting all the logical parcels
        $logical_parcels = LogicalParcel::all();
        if(!$logical_parcels) return response("Not Found");
        
        /* PARCELS
        "id": 11,
        "agricultural_cycle": null,
        "code_ormva": null,
        "annuel_surface": "5",
        "tenure": "property",
        "exploited_surface": "20",
        "manuel_surface": "130",
        "gps_surface": "122",
        "harvested_surface": "178",
        "abandoned_surface": "12",
        "cleared_surface": "16",
        "stricken_surface": "12",
        "irrigation_mode": null,
        "coordinate": null,
        "costum_fields": null,
        "soil_id": 17,
        "contract_id": 14,
        "logical_parcel_id": 1,
        "parent_id": 1
        */
        
        /* LOGICAL 
        "id": 2,
        "name": "lp2",
        "parent_id": 0
        */
        
        
        // dd($logical_parcels);
       // return $logical_parcels;
       /*
        $parcel_arr = [];
        foreach($logical_parcels as $l_p){
            $l_p['parent_id'] = 0;
            $l_p['is_selected'] = false;
            $cloned_l_p = clone $l_p;
            $cloned_l_p_soil = clone $l_p;
            $cloned_l_p['cda'] = $cloned_l_p_soil->parcels[0]->soil->cda;
            $cloned_l_p['zone'] = $cloned_l_p_soil->parcels[0]->soil->zone;
            $cloned_l_p['registration_number'] = null;
            $cloned_l_p['cin'] = $l_p->third_party->cin;
            $cloned_l_p['full_name'] = $l_p->third_party->full_name;
            $cloned_l_p['code_ormva'] = null;
            $cloned_l_p['tenure'] = null;
            $cloned_l_p['status'] = 'aa';//$l_p->contract->status;
            // $third_party_type = DB::select("select type from third_party_structures where type = 'aggregated' and third_party_id = " . $l_p->third_party_id);
            // $third_party_type = $l_p->third_party->thirdPartyStructures[0]->type;
            // dd($third_party_type);
            $cloned_l_p['third_party_type'] = 'Agriculteurr';
            // $cloned_l_p['status'] = $l_p->contract->status;
            // $cloned_l_p['campaign_surface'] = $l_p->contract->campaign_surface;
            // $cloned_l_p['ref_contract'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $l_p->contract->application_date) . '/' . $l_p->contract_id;
            
            $parcel_arr[] = $cloned_l_p;
            
            $physical_parcels = $l_p->parcels;
            
            foreach($physical_parcels as $ph_p){
                $ph_p['parent_id'] =  $l_p->id;
                $ph_p['third_party_id'] =  $l_p->third_party_id;
                $ph_p['name'] =  $l_p->name;
                $ph_p['name'] =  $l_p->name;
                $ph_p['id'] = $ph_p['id']*658;
                $ph_p['is_selected'] = false;
                $clone_ph_p = clone $ph_p;
                $clone_ph_p['cda'] = $ph_p->soil->cda;
                $clone_ph_p['zone'] = $ph_p->soil->zone;
                $clone_ph_p['registration_number'] = $ph_p->soil->registration_number;
                $clone_ph_p['cin'] = $l_p->third_party->cin;
                $clone_ph_p['full_name'] = $l_p->third_party->full_name;
                $clone_ph_p['third_party_type'] = 'Agriculteur';
                $clone_ph_p['status'] = 'zz';// $ph_p->contract->status;
                // $clone_ph_p['status'] = $ph_p->contract->status;
                // $clone_ph_p['campaign_surface'] = $ph_p->contract->campaign_surface;
                // $clone_ph_p['ref_contract'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ph_p->contract->application_date) . '/' . $ph_p->contract_id;
                $parcel_arr[] = $clone_ph_p;
            }
            //return  $l_p->parcelsSelect;
        }
        return  $parcel_arr;
    }
    */
    
}