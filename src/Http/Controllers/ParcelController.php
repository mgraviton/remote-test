<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Http\Controllers;

use AgreementModule;
use Illuminate\Http\Request;
use SIAM618\Core\Support\Http\Controller;
use SIAM618\Agreement\Database\DataGrids\ParcelDataGrid;
use SIAM618\Agreement\Database\DataGrids\HarvestOrderDataGrid;
use SIAM618\Agreement\Database\DataGrids\TodosDataGrid;
use SIAM618\Agreement\Database\Models\Parcel;
use SIAM618\Agreement\Database\Models\Soil;
use SIAM618\Agreement\Database\Models\LogicalParcel;
use SIAM618\Agreement\Http\Resources\ParcelResource;
use SIAM618\Agreement\Http\Resources\LogicalParcelResource;
use SIAM618\Agreement\Http\Resources\ParcelDataGridResource;
use SIAM618\Agreement\Services\ParcelService;
use SIAM618\Agreement\Http\Requests\Parcel\StoreRequest;
use SIAM618\Agreement\Http\Requests\Parcel\UpdateRequest;
use SIAM618\Agreement\Http\Requests\Parcel\ParcelByStructureRequest;
use SIAM618\Agreement\Database\Models\Contract;
use SIAM618\Base\Database\Models\Zone;
use SIAM618\Core\Support\CSV\CsvImporter;
use DB;
use SIAM618\Agreement\Services\ContractService;

class ParcelController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param AgreementService $service
     * @param AgreementDataGrid $dataGrid
     * @return Response
     */
    public function grid(ParcelDataGrid $dataGrid)
    {
        return $dataGrid->resource();
    }

    /**
     * @param HarvestConvocationDataGrid $dataGrid
     * @return mixed
     */
    public function harvestOrderGrid(HarvestOrderDataGrid $dataGrid){

        return $dataGrid->resource();

    }

    /**
     * @param TodosDataGrid $dataGrid
     * @return mixed
     */
    public function todosGrid(TodosDataGrid $dataGrid)
    {
        return $dataGrid->resource();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $parcels = collect($request->all())->groupBy('code_ormva')->toArray();
        foreach ($parcels as $parcel) {
            foreach ($parcel as $p) {
                /*** Tester si le sol exist ***/
                $soil = DB::table('soils')->whereId($p['soil_id'])->first();
                $parcelsCheck = Parcel::whereSoilId($soil->id)->get()->filter(function ($soil_parcel) use ($p, $soil) {
                    $contract = DB::table('contracts')->whereId($soil_parcel->contract_id)->first();
                    $campaign = DB::table('campaigns')->whereId($soil_parcel->campaign_id)->first();
                    $status = (isset($p['parcel_tmp_id'])) ? false : $contract->status !== 'inprogress';
                    if ($campaign->structure_id == 7 || $campaign->structure_id == 8) {
                        $current_conract = DB::table('contracts')->whereId($p['contract_id'])->first();
                        return $campaign->end_date === null
                        && $soil_parcel->deleted_at === null
                        && $status
                        && \Carbon\Carbon::parse($contract->start_date)->diffInYears(\Carbon\Carbon::parse($current_conract->start_date)) == 0;
                    }
                    return $campaign->end_date === null
                    && $status
                    && $soil_parcel->deleted_at === null;
                });
                if ($parcelsCheck->count() > 0) {
                    return response()->error(409, "La parcelle est déjà contractée dans cette campagne.");
                }
                /*** Fin Test d'existance du sol ***/
            }
        }
        foreach ($parcels as $key => $parcel) {
            $parcel_logique = [];
            $is_edit = false;
            $pp = null;
            $parcel_logique['annuel_surfaces'] = 0;
            $ps = array_map(function ($p) use (&$parcel_logique, &$is_edit, &$pp, $key) {
                if(!$key) {
                    $parcel_logique['annuel_surfaces'] = floatval($p['annuel_surface']);
                }
                else {
                    $parcel_logique['annuel_surfaces'] += floatval($p['annuel_surface']);
                }
                $parcel_logique['exploited_surface'] = $parcel_logique['annuel_surfaces'];
                $parcel_logique['manuel_surface'] = $parcel_logique['annuel_surfaces'];
                $parcel_logique['gps_surface'] = $parcel_logique['annuel_surfaces'];
                $parcel_logique['manuel_surface'] = $parcel_logique['annuel_surfaces'];
                $parcel_logique['code_ormva'] = $p['code_ormva'] ? $p['code_ormva'] : $p['registration_number'];
                $parcel_logique['name'] = $parcel_logique['code_ormva'];
                $contract = Contract::find($p['contract_id']);
                $parcel_logique['third_party_id'] = $contract->third_party_id;
                $parcel_logique['zone_id'] = $p['zone_id'];
                $parcel_logique['soil_id'] = $p['soil_id'];
                $parcel_logique['contract_id'] = $p['contract_id'];
                $parcel_logique['is_logical'] = true;
                $parcel_logique['campaign_id'] = $p['campaign_id'];
                if (isset($p['parcel_tmp_id']) && !$contract->parent_id) {
                    $is_edit = true;
                    $p1 = $pp = Parcel::find($p['parcel_tmp_id']);
                } else {
                    $p1 = new Parcel;
                }
                $p1->fill($p)->save();
                if (!$key) {
                    if ($is_edit) {
                        $logical_parcel = $pp->logical_parcel;
                    } else {
                        $logical_parcel = new Parcel;
                    }
                    $parcel_logique['annuel_surface'] = floatval($p['annuel_surface']);

                    $logical_parcel->fill($parcel_logique);

                    $logical_parcel->save();

                    $tmp = Parcel::find($p1['id']);
                    $tmp->parcel_id = $logical_parcel->id;
                    $tmp->save();
                }
                return $p1;
            }, $parcel);
            if ($key) {
                if ($is_edit) {
                    $logical_parcel = $pp->logical_parcel;
                } else {
                    $logical_parcel = new Parcel;
                }
                $parcel_logique['annuel_surface'] = $parcel_logique['annuel_surfaces'];
                $logical_parcel->fill($parcel_logique);
                $logical_parcel->save();
                array_map(function ($p) use ($logical_parcel) {
                    $tmp = Parcel::find($p['id']);
                    $tmp->parcel_id = $logical_parcel->id;
                    $tmp->save();
                    return $p;
                }, $ps);
            }
        }
        return response()->success('La parcelle a été créer!');
    }

    public function storeLogical(Request $request)
    {
        $logical_parcel = new Parcel;
        $logical_parcel->fill($request->parcel)->save();
        $logical_parcel->exploited_surface = $logical_parcel->annuel_surface;
        $logical_parcel->annuel_surface = $logical_parcel->annuel_surface;
        $logical_parcel->gps_surface = $logical_parcel->annuel_surface;
        $logical_parcel->manuel_surface = $logical_parcel->annuel_surface;
        $logical_parcel->save();

        foreach ($request->parcels as $parcel) {
            $p = Parcel::find($parcel['parcel_tmp_id']);
            $p->parcel_id = $logical_parcel->id;
            $p->save();
        }
        return response()->successStore($logical_parcel, ParcelResource::class);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $model
     * @return Response
     */
    public function show(Parcel $parcel)
    {
        return response()->item($parcel, ParcelResource::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $model
     * @return Response
     */
    public function update(UpdateRequest $request, Parcel $parcel)
    {
        $parcel->fill($request->all())->save();
        $parcel->exploited_surface = $parcel->annuel_surface;
        $parcel->annuel_surface = $parcel->annuel_surface;
        $parcel->gps_surface = $parcel->annuel_surface;
        $parcel->manuel_surface = $parcel->annuel_surface;
        $parcel->save();


        $logical = $parcel->logical_parcel;
        $annuel_surfaces = 0;
        foreach ($logical->parcels as $p) {
            $annuel_surfaces += floatval($p->annuel_surface);
        }
        $logical->exploited_surface = $annuel_surfaces;
        $logical->annuel_surface = $annuel_surfaces;
        $logical->gps_surface = $annuel_surfaces;
        $logical->manuel_surface = $annuel_surfaces;
        $logical->save();

        return response()->successUpdate($parcel, ParcelResource::class);
    }

    public function updateMass(Request $request, Parcel $parcel)
    {
        $parcels = collect($request->all())->groupBy('code_ormva')->toArray();
        foreach ($parcels as $parcel) {
            $parcel_logique = [];
            $parcel_logique['annuel_surfaces'] = 0;
            $ps = array_map(function ($p) use (&$parcel_logique, $request) {
                /*** Tester si le sol exist ***/
                $soil = Soil::find($p['soil_id']);
                $parcelsCheck = $soil->parcels->filter(function ($parcel) use ($request) {
                    $contract = $parcel->contract;
                    $campaign = $contract->campaign;
                    return $campaign->end_date === null && $parcel->code_ormva === $request->code_ormva && $parcel->deleted_at === null;
                });

                if ($parcelsCheck->count() > 0) {
                    return response()->error(409, "La parcelle est déjà contractée dans cette campagne.");
                }
                /*** Fin Test d'existance du sol ***/

                $parcel_logique['annuel_surfaces'] += intval($p['annuel_surface']);
                $parcel_logique['exploited_surface'] = $parcel_logique['annuel_surfaces'];
                $parcel_logique['manuel_surface'] = $parcel_logique['annuel_surfaces'];
                $parcel_logique['gps_surface'] = $parcel_logique['annuel_surfaces'];
                $parcel_logique['manuel_surface'] = $parcel_logique['annuel_surfaces'];
                $parcel_logique['code_ormva'] = $p['code_ormva'] ? $p['code_ormva'] : $p['registration_number'];
                $parcel_logique['name'] = $p['code_ormva'];
                $parcel_logique['third_party_id'] = user_id();
                $parcel_logique['zone_id'] = $p['zone'];
                $parcel_logique['soil_id'] = $p['soil_id'];
                $parcel_logique['contract_id'] = $p['contract_id'];
                $parcel_logique['is_logical'] = true;

                $p1 = Parcel::find($p['parcel_tmp_id']);
                $p1->fill($p)->save();
                return $p1;
            }, $parcel);

            /*$logical_parcel = Parcel::find($p['parcel_tmp_id']);
            $parcel_logique['annuel_surface'] = $parcel_logique['annuel_surfaces'];
            $logical_parcel->fill($parcel_logique);
            $logical_parcel->save();
            array_map(function ($p) use ($logical_parcel) {
                $tmp = Parcel::find($p['id']);
                $tmp->parcel_id = $logical_parcel->id;
                $tmp->save();
                return $p;
            }, $ps);*/
        }

        return response()->success('ok');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $model
     * @return Response
     */
    public function destroy(Parcel $parcel)
    {
        $logical = $parcel->logical_parcel;
        $parcel->delete();
        $annuel_surfaces = 0;
        if ($logical->parcels->count() == 0) {
            Parcel::find($logical->id)->delete();
        } else {
            foreach ($logical->parcels as $p) {
                if ($p->id !== $parcel->id) {
                    $annuel_surfaces += floatval($p->annuel_surface);
                }
            }
            $logical->exploited_surface = $annuel_surfaces;
            $logical->annuel_surface = $annuel_surfaces;
            $logical->gps_surface = $annuel_surfaces;
            $logical->manuel_surface = $annuel_surfaces;
            $logical->save();
        }
        return response()->successDelete();
    }


    public function getLPByUser($id, ContractService $service)
    {
        $user_id = user_id();
        //$contrat_avenant = $this->getContractOrAvenant($service, $id);
        $lp = DB::table('parcels AS p')
            ->join('contracts AS c', 'p.contract_id', '=', 'c.id')
            ->join('third_parties AS tp', 'tp.id', '=', 'p.third_party_id')
            ->join('zones AS z', 'z.id', '=', 'p.zone_id')
            ->whereIn('p.zone_id', function ($query) use ($user_id) {
                $query->select('zone_id')
                    ->from('user_zones')
                    ->where('user_id', $user_id);
            })
            ->where('p.third_party_id', $id)
            ->where('c.status', 'actif')
            //->where('c.structure_id', user_division_id())

            ->where('p.is_logical', true)
            ->whereNull('p.deleted_at')
            ->where('c.campaign_id', $service->currentCampaign(user_division_id()))
//            ->where('p.contract_id',$contrat_avenant->id)

            ->select('p.*', 'tp.code', 'tp.cin', 'tp.full_name', 'z.name as zone_name')
            ->get();

        if (!$lp) {
            return response('Not Found');
        }

        return [
            'data' => $lp,
        ];
    }

    public function getContractOrAvenant(ContractService $service, $tp_id = null)
    {
        $contract = DB::table('contracts as c')
            ->where('c.campaign_id', $service->currentCampaign(user_division_id()))
            ->where('c.status', 'actif')

            ->select('c.id as id')
            ->orderBy('c.id', 'desc');

        if($tp_id) $contract = $contract->where('c.third_party_id', $tp_id)->take(1)->first();
        else $contract = $contract->take(1)->first();
        return $contract;
    }

    public function getCDAByZone($id) {
        $query = "select cda.name as cda_name from zones z, zones cda where cda.type = 'cda' and cda.tree_path @> z.tree_path  and z.id = " . $id;
        $cda =  DB::SELECT($query);
        return [
            "data" => $cda
        ];
    }

    /*
        public function _getAllLogicalAndPhysicalParcels()
        {
            // Getting all the logical parcels
            $logical_parcels = Parcel::whereNull('parcel_id')->get();
            // dd(count($logical_parcels));
            if(!$logical_parcels) return response()->error(404, "Not Found");

            /*{ PARCELS
            "id": 11,
            "agricultural_cycle": null,
            "code_ormva": null,
            "annuel_surface": "5",
            "tenure": "property",
            "exploited_surface": "20",
            "manuel_surface": "130",
            "gps_surface": "122",
            "harvested_surface": "178",
            "abandoned_surface": "12",
            "cleared_surface": "16",
            "stricken_surface": "12",
            "irrigation_mode": null,
            "coordinate": null,
            "costum_fields": null,
            "soil_id": 17,
            "contract_id": 14,
            "logical_parcel_id": 1,
            "parent_id": 1
            }*/

    /*{ LOGICAL
    "id": 2,
    "name": "lp2",
    "parent_id": 0
    }*/


    // dd($logical_parcels);
    // return $logical_parcels;
    /*$parcel_arr = [];
    foreach($logical_parcels as $l_p){
        $l_p['parent_id'] = 0;
        $l_p['is_selected'] = false;
        $cloned_l_p = clone $l_p;
        $cloned_l_p_soil = clone $l_p;
        if(count($cloned_l_p_soil->physical_parcels) != 0) {
            $cloned_l_p['cda'] = $cloned_l_p_soil->cda;
            $cloned_l_p['zone'] = $cloned_l_p_soil->zone;
            $cloned_l_p['registration_number'] = null;
            $cloned_l_p['cin'] = $l_p->third_party->cin;
            $cloned_l_p['full_name'] = $l_p->third_party->full_name;
            $cloned_l_p['code_ormva'] = null;
            $cloned_l_p['tenure'] = null;
            $cloned_l_p['status'] = $l_p->contract->status;
            // $third_party_type = DB::select("select type from third_party_structures where type = 'aggregated' and third_party_id = " . $l_p->third_party_id);
            // $third_party_type = $l_p->third_party->thirdPartyStructures[0]->type;
            // dd($third_party_type);
            $cloned_l_p['third_party_type'] = 'Agriculteurr';
            // $cloned_l_p['status'] = $l_p->contract->status;
            // $cloned_l_p['campaign_surface'] = $l_p->contract->campaign_surface;
            // $cloned_l_p['ref_contract'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $l_p->contract->application_date) . '/' . $l_p->contract_id;

        }

        $parcel_arr[] = $cloned_l_p;

        $physical_parcels = $l_p->physical_parcels;

        foreach($physical_parcels as $ph_p){
            $ph_p['parent_id'] =  $l_p->id;
            $ph_p['third_party_id'] =  $l_p->third_party_id;
            $ph_p['name'] =  $l_p->name;
            $ph_p['id'] = $ph_p['id'];
            $ph_p['is_selected'] = false;
            $clone_ph_p = clone $ph_p;
            $clone_ph_p['cda'] = $ph_p->cda;
            $clone_ph_p['zone'] = $ph_p->zone;
            $clone_ph_p['registration_number'] = $ph_p->soil->registration_number;
            $clone_ph_p['cin'] = $l_p->third_party->cin;
            $clone_ph_p['full_name'] = $l_p->third_party->full_name;
            $clone_ph_p['third_party_type'] = 'Agriculteur';
            $clone_ph_p['status'] = $ph_p->contract->status;
            // $clone_ph_p['status'] = $ph_p->contract->status;
            // $clone_ph_p['campaign_surface'] = $ph_p->contract->campaign_surface;
            // $clone_ph_p['ref_contract'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ph_p->contract->application_date) . '/' . $ph_p->contract_id;
            $parcel_arr[] = $clone_ph_p;

        }
        //return  $l_p->parcelsSelect;
    }
    return  $parcel_arr;
}
*/
    public function getAllLogicalAndPhysicalParcels(ContractService $service)
    {
        $contrat_avenant = $this->getContractOrAvenant($service);

        $logical_parcels = Parcel::where('is_logical', true)->whereHas('contract', function($query) use ($service) {
            $query->where('structure_id', user_division_id())
                ->where('campaign_id', $service->currentCampaign(user_division_id()))
                ->where('status', 'actif');

        })->get();
//        return response()->error(404, 'eee',$logical_parcels);
        if ($logical_parcels == null)   return response()->error(404, 'Parcels Logiques Introuvable !!');
        $parcel_arr = [];
        foreach ($logical_parcels as $l_p) {
            $l_p['parent_id'] = 0;
            $l_p['is_selected'] = false;
            $cloned_l_p = clone $l_p;
            // $cloned_l_p_soil = clone $l_p;
            $cloned_l_p['cda'] = Zone::find($l_p->soil->cda)->name; //$cloned_l_p_soil->cda;
            $cloned_l_p['zone'] = Zone::find($l_p->soil->zone)->name; //$cloned_l_p_soil->zone;
            $cloned_l_p['registration_number'] = $l_p->soil->registration_number;
            $cloned_l_p['cin'] = $l_p->third_party->cin;
            $cloned_l_p['full_name'] = $l_p->third_party->full_name;
            $cloned_l_p['code_ormva'] = $l_p->code_ormva;
            $cloned_l_p['tenure'] = null;
            $contract = Contract::find($l_p['contract_id']);
            // $cloned_l_p['status'] = $contract->status;
            $cloned_l_p['third_party_type'] = 'Agriculteur';
            // $cloned_l_p['status'] = $l_p->contract->status;
            // $cloned_l_p['campaign_surface'] = $l_p->contract->campaign_surface;
            // $cloned_l_p['ref_contract'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $l_p->contract->application_date) . '/' . $l_p->contract_id;
            $contract = $l_p->contract == null ? Contract::find($l_p['contract_id']) : $l_p->contract;
            if($contract == null) return response()->error(404, "Contrat Introuvable !!");
            else $cloned_l_p['contract_code'] = $contract->code;
            $parcel_arr[] = $cloned_l_p;

            $physical_parcels = $l_p->parcels;
            // dump('id :'.$l_p->id.' +++ '.count($physical_parcels));
            if (count($physical_parcels) != 0) {
                foreach ($physical_parcels as $ph_p) {
                    $ph_p['parent_id'] = $l_p->id;
                    $ph_p['third_party_id'] = $l_p->third_party_id;
                    $ph_p['name'] = $ph_p->name;
                    $ph_p['is_selected'] = false;
                    $clone_ph_p = clone $ph_p;
                    $clone_ph_p['cda'] = Zone::find($ph_p->soil->cda)->name;
                    $clone_ph_p['zone'] = Zone::find($ph_p->soil->zone)->name;//$ph_p->zone;
                    $clone_ph_p['registration_number'] = $ph_p->soil->registration_number;
                    $clone_ph_p['cin'] = $l_p->third_party->cin;
                    $clone_ph_p['full_name'] = $l_p->third_party->full_name;
                    $clone_ph_p['third_party_type'] = 'Agriculteur';
                    $clone_ph_p['status'] = $ph_p->contract !== null ? $ph_p->contract->status : Contract::find($l_p['contract_id'])->status;
                    // $clone_ph_p['status'] = $ph_p->contract->status;
                    // $clone_ph_p['campaign_surface'] = $ph_p->contract->campaign_surface;
                    // $clone_ph_p['ref_contract'] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $ph_p->contract->application_date) . '/' . $ph_p->contract_id;
                    $parcel_arr[] = $clone_ph_p;
                }
            }
            //return  $l_p->parcelsSelect;
        }
        return  $parcel_arr;
    }


    function getParcelsByStructure1(ParcelByStructureRequest $req){
        /*$structures = $req->explode(',');

        foreach($structures as $str_id){
           DB::tabel('parcels as p')
            ->join('soils as s2', 'p.soil_id', '=', 's2.id')
            ->join('zones as z', 's2.zone', '=', 'z.id')
            ->join('zones as z2', 's2.cda', '=', 'z2.id')
            ->join('contracts as c2', 'p.contract_id', '=', 'c2.id')
            ->join('third_parties as tp', 'c2.third_party_id', '=', 'tp.id')
            ->join('cards as c3', 'tp.id', '=', 'c3.third_party_id')
            ->where('p.is_logical', true)
            ->where('c3.status', 'active')
            ->where('c2.structure_id', $str_id)
            ->where('c3.structure_id', $str_id)
            ->where('c3.updated_at', '>', $lst_str)
            ->select('tp.cin',
                     'tp.full_name',
                     'c3.rfid',
                     'p.name',
                     'p.annuel_surface',
                     'z2.name as cda',
                     'z.name as zone');
        }*/



    }


    function getParcelsByStructure(Request $request){


        $structures =$request->structuresList;

        $_array = [];

        $str = [];

        $str = explode(",",$structures);



        foreach((array)$str as $str_id){

            /*$parcels = DB::table('parcels as p')
              ->join('soils as s2', 'p.soil_id', '=', 's2.id')
              ->join('zones as z', 's2.zone', '=', 'z.id')
              ->join('zones as z2', 's2.cda', '=', 'z2.id')
              ->join('contracts as c2', 'p.contract_id', '=', 'c2.id')
              ->join('third_parties as tp', 'c2.third_party_id', '=', 'tp.id')
              ->join('cards as c3', 'tp.id', '=', 'c3.third_party_id')
                ->select('tp.cin',
                    'tp.full_name',
                    'c3.rfid',
                    'p.name',
                    'p.annuel_surface',
                    'z2.name as cda',
                    'z.name as zone')
              ->where('p.is_logical', true)
              ->where('c3.status', 'active')
              ->where('c2.structure_id', $str_id)
                ->whereNotNull('c3.rfid')
                ->whereNotNull('tp.cin')


                ->where('c3.structure_id', $str_id)
              ->where('c3.updated_at', '>', $request->date)
              ->get();


             $_array[] = $parcels;*/
            $parcels = DB::table('parcels as p')
                ->join('soils as s2', 'p.soil_id', '=', 's2.id')
                ->join('zones as z', 's2.zone', '=', 'z.id')
                ->join('zones as z2', 's2.cda', '=', 'z2.id')
                ->join('contracts as c2', 'p.contract_id', '=', 'c2.id')
                ->join('third_parties as tp', 'c2.third_party_id', '=', 'tp.id')
                ->join('cards as c3', 'tp.id', '=', 'c3.third_party_id')
                ->select('tp.cin',
                    'tp.full_name',
                    'c3.rfid',
                    'p.name',
                    'p.annuel_surface',
                    'z2.name as cda',
                    'z.name as zone')
                ->where('p.is_logical', true)
                ->where('c3.status', 'active')
                ->whereNotNull('c3.rfid')
                ->whereNotNull('tp.cin')


                ->where('c3.structure_id', $str_id)
                // ->where('c3.updated_at', '>', $request->date)
                ->get();


            $_array[] = $parcels;

        }


        return ["data" => $_array ];

    }




}
