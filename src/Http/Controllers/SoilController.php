<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Http\Controllers;

use AgreementModule;
use SIAM618\Core\Support\Http\Controller;
use SIAM618\Agreement\Database\DataGrids\SoilDataGrid;
use SIAM618\Agreement\Database\Models\Soil;
use SIAM618\Agreement\Http\Resources\SoilResource;
use SIAM618\Agreement\Http\Resources\SoilDataGridResource;
use SIAM618\Agreement\Services\SoilService;
use SIAM618\Agreement\Http\Requests\Soil\StoreRequest;
use SIAM618\Agreement\Http\Requests\Soil\UpdateRequest;

class SoilController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param AgreementService $service
     * @param AgreementDataGrid $dataGrid
     * @return Response
     */
    public function grid(SoilDataGrid $dataGrid)
    {
        return $dataGrid->resource();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(StoreRequest $request, SoilService $service)
    {
        $s = Soil::where('cda', $request->cda)->where('zone', $request->zone)
            ->where('registration_number', $request->registration_number)->first();
        if ($s) {
            $s->update($request->all());
            return response()->successStore($s, SoilResource::class);
        }
        $soil = new Soil;
        $soil->fill($request->all())->save();
        return response()->successStore($soil, SoilResource::class);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $model
     * @return Response
     */
    public function show(Soil $soil)
    {
        return response()->item($soil, SoilResource::class);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $model
     * @return Response
     */
    public function update(UpdateRequest $request, Soil $soil)
    {
        $soil->fill($request->all())->save();
        return response()->successUpdate($soil, SoilResource::class);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $model
     * @return Response
     */
    public function destroy(Soil $soil)
    {
        $soil->delete();
        return response()->successDelete();
    }
}