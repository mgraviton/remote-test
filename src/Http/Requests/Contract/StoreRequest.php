<?php

namespace SIAM618\Agreement\Http\Requests\Contract;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'application_date' => 'date',
            'signature_date' => 'date',
            'expiration_date' => 'date|after:application_date',
            'type' => 'enum:agreement.contract_types',
            'third_party_id' => 'required|integer',
            'structure_id' => 'required|integer',
        ];
    }
}
