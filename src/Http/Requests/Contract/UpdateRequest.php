<?php

namespace SIAM618\Agreement\Http\Requests\Contract;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'application_date' => 'date',
            'signature_date' => 'date',
            'expiration_date' => 'date|after:application_date',
            'code_ormva' => 'max:20',
            'code_siam' => 'max:20',
            /*'culture_type' => 'required|in:cas,bas',*/
            'contrat_type' => 'in:pluriannuel,annuel,avenant'
        ];
    }
}
