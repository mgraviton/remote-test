<?php

namespace SIAM618\Agreement\Http\Requests\Card;

use Illuminate\Foundation\Http\FormRequest;

class GenerateCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rfid' => 'required|max:100',
            'type' => 'required|in:agri',
            'face' => 'required|in:recto,verso'
        ];
    }
}
