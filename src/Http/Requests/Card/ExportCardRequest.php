<?php

namespace SIAM618\Agreement\Http\Requests\Card;

use Illuminate\Foundation\Http\FormRequest;

class ExportCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cards.*.id' => 'required|numeric',
            'cards.*.rfid' => 'required|max:100'
        ];
    }
}
