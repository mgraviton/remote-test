<?php

namespace SIAM618\Agreement\Http\Requests\LogicalParcel;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'annuel_surface'    => 'nullable|numeric',
            'exploited_surface' => 'nullable|numeric',
            'manuel_surface'    => 'nullable|numeric',
            'gps_surface'       => 'nullable|numeric',
            'harvested_surface' => 'nullable|numeric',
            'abandoned_surface' => 'nullable|numeric',
            'cleared_surface'   => 'nullable|numeric',
            'stricken_surface'  => 'nullable|numeric',
            'contract_id'       => 'integer|exists:contracts,id'
        ];
    }
}
