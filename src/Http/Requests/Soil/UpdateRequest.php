<?php

namespace SIAM618\Agreement\Http\Requests\Soil;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'registration_number' => 'sometimes|required|max:50',
            'common_rural' => 'max:50',
            'perimeter' => 'max:50',
            'region' => 'max:50',
            'district' => 'max:50',
            'cda' => 'sometimes|required|max:50',
            'zone' => 'sometimes|required|max:50',
            'secteur' => 'max:50',
            'block' => 'max:50',
            'total_surface' => 'Numeric',
            'bare_surface' => 'Numeric',
            'identified_surface' => 'Numeric',
            'soil_number' => 'Integer',
            'culture_number' => 'Integer',
            'legal_status' => 'max:20'
        ];
    }
}
