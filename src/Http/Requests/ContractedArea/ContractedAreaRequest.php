<?php

namespace SIAM618\Agreement\Http\Requests\ContractedArea;

use Illuminate\Foundation\Http\FormRequest;

class ContractedAreaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contracted_surfaces' => 'required',
            'contracted_surfaces.*.contracted_surface' => 'required|numeric',
            'contracted_surfaces.*.agreement_id' => 'required|integer',
            'contracted_surfaces.*.campaign_id' => 'required|integer',
        ];
    }
}
