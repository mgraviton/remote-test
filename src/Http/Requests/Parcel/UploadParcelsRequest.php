<?php

namespace SIAM618\Agreement\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class UploadParcelsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'csv' => 'required|file|mimes:csv,txt'
        ];
    }
}
