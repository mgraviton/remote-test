<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HarvestOrderDataGridResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'p_id' => $this->p_id,
            'p_agricultural_cycle' => $this->p_agricultural_cycle,
            'p_code_ormva' => $this->p_code_ormva,
            'p_annuel_surface' => $this->p_annuel_surface,
            'p_tenure' => $this->p_tenure,
            'p_name' => $this->p_name,
            'p_harvest_order' => $this->p_harvest_order,
            'a_id' => $this->a_id,
            'a_application_date' => $this->a_application_date,
            'a_type' => $this->a_type,
            'a_contracted_surface' => $this->a_contracted_surface,
            'a_compaign_surface' => $this->a_compaign_surface,
            'a_code' => $this->a_code,
            'a_structure_id' => $this->a_structure_id,
            'tp_id' => $this->tp_id,
            'tp_full_name' => $this->tp_full_name,
            'tp_full_name_ar' => $this->tp_full_name_ar,
            'tp_cin' => $this->tp_cin,
            'tp_email' => $this->tp_email,
            'tp_birth_date' => $this->tp_birth_date,
            'tp_death_date' => $this->tp_death_date,
            'tp_sexe' => $this->tp_sexe,
            'tp_address' => $this->tp_address,
            'tp_address_ar' => $this->tp_address_ar,
            'tp_city' => $this->tp_city,
            'tp_district' => $this->tp_district,
            'tp_commandment' => $this->tp_commandment,
            'tp_commune' => $this->tp_commune,
            'tp_region' => $this->tp_region,
            'tp_zip_code' => $this->tp_zip_code,
            'tp_tel1' => $this->tp_tel1,
            'tp_civil_status' => $this->tp_civil_status,
            'tp_is_insured' => $this->tp_is_insured,
            'tp_is_retired' => $this->tp_is_retired,
            'tp_payment_method' => $this->tp_payment_method,
            'tp_bank_name' => $this->tp_bank_name,
            'tp_code' => $this->tp_code,
            'tp_rib' => $this->tp_rib,
            'cmp_id' => $this->cmp_id,
            'cmp_description' => $this->cmp_description,
            'cmp_estimated_surface' => $this->cmp_estimated_surface,
            'cmp_estimated_tonnage' => $this->cmp_estimated_tonnage,
            'z_name' => $this->z_name,
            'z_code' => $this->z_code,
            'cda_code' => $this->cda_code,
            'cda_name' => $this->cda_name,
            'sup_semi' => $this->sup_semis
        ];
    }
}
