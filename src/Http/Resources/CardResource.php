<?php

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (String) $this->id,
            'rfid' => $this->code_rfid,
            'status' => $this->status,
            'description' => $this->description,
            'printed_at' => $this->printed_at,
            'third_party_id' => $this->third_party_id
            /*'structure_id' =>
            'third_party_id' =>*/
            ];
    }
}