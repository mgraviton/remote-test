<?php

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use SIAM618\ThirdParty\Http\Resources\ThirdParty\ThirdPartyIdentifierResource;
use SIAM618\Base\Http\Resources\Structure\StructureIdentifierResource;
use SIAM618\Base\Http\Resources\Campaign\CampaignIdentifierResource;
use SIAM618\Agreement\Http\Resources\LogicalParcel\LogicalParcelIdentifierResource;

class ContractResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'code' => $this->code,
            'application_date' => $this->application_date,
            'expiration_date' => $this->expiration_date,
            'signature_date' => $this->signature_date,
            'status' => $this->status,
            'parent_id' => $this->parent_id,
            'type' => !$request->has('edit') ? enum('agreement.contract_types', $this->type) : $this->type,
            'status_value' => !$request->has('edit') ? enum('agreement.contract_status', $this->status) : $this->status,
            'contracted_surface' => $this->contracted_surface,
            'compaign_surface' => $this->compaign_surface,
            'third_party' => new ThirdPartyIdentifierResource($this->thirdParty),
            'structure' => new StructureIdentifierResource($this->structure),
            'campaign' => new CampaignIdentifierResource($this->campaign),
            'documents' => DocumentIdentifierResource::collection($this->documents),
            'amendments' => ContractIdentifierResource::collection($this->amendments),
            'parcels' => ParcelIdentifierResource::collection($this->parcels)
        ];
    }
}
