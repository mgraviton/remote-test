<?php

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use SIAM618\ThirdParty\Http\Resources\ThirdParty\ThirdPartyIdentifierResource;
use SIAM618\Base\Http\Resources\Structure\StructureIdentifierResource;

class ContractIdentifierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => (string) $this->id,
            'code'                  => $this->code,
            'parent_id'             => $this->parent_id,
            'application_date'      => $this->application_date,
            'expiration_date'       => $this->expiration_date,
            'signature_date'        => $this->signature_date,
            'status'                => $this->status,
            'contracted_surface'    => $this->contracted_surface,
            'parcels'               => ParcelIdentifierResource::collection($this->parcels),
        ];
    }
}
