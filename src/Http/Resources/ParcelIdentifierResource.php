<?php

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ParcelIdentifierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'agricultural_cycle' => $this->agricultural_cycle,
            'code_ormva' => $this->code_ormva,
            'name' => $this->name,
            'is_logical' => $this->is_logical,
            'parcel_id' => $this->parcel_id,
            'annuel_surface' => $this->annuel_surface,
            'tenure' => enum('agreement.tenure', $this->tenure),
            'tenure_id' => $this->tenure,
            'exploited_surface' => $this->exploited_surface,
            'manuel_surface' => $this->manuel_surface,
            'gps_surface' => $this->gps_surface,
            'harvested_surface' => $this->harvested_surface,
            'abandoned_surface' => $this->abandoned_surface,
            'cleared_surface' => $this->cleared_surface,
            'stricken_surface' => $this->stricken_surface,
            'irrigation_mode' => enum('agreement.irrigation_mode', $this->irrigation_mode),
            'soil' => new SoilIdentifierResource($this->soil),
            'parcels' => ParcelIdentifierResource::collection($this->parcels)
        ];
    }
}
