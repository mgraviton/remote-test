<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */
 
namespace SIAM618\Agreement\Http\Resources\LogicalParcel;

use Illuminate\Http\Resources\Json\JsonResource;

class LogicalParcelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'name'                  =>  $this->name,
            'annuel_surface'        => $this->annuel_surface,
            'exploited_surface'     => $this->exploited_surface,
            'manuel_surface'        => $this->manuel_surface,
            'gps_surface'           => $this->gps_surface,
            'harvested_surface'     => $this->harvested_surface,
            'abandoned_surface'     => $this->abandoned_surface,
            'cleared_surface'       => $this->cleared_surface,
            'stricken_surface'      => $this->stricken_surface,
            'zone_id'               => $this->zone_id,
            'campaign_id'           => $this->campaign_id,
            'contract_id'              => $this->contract_id,
            'third_party_id'        => $this->third_party_id,
            'third_party_code'      =>  ag_code($this->third_party_id),
            // 'parcels'            => ParcelIdentifierResource::collection($this->parcels)
        ];
    }
}
