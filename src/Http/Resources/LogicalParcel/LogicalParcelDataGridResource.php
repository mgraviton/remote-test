<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Http\Resources\LogicalParcel;

use Illuminate\Http\Resources\Json\JsonResource;

class LogicalParcelDataGridResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    =>  $this->id,
            'name'                  =>  $this->name,
            'annuel_surface'        =>  $this->annuel_surface,
            'exploited_surface'     =>  $this->exploited_surface,
            'manuel_surface'        =>  $this->manuel_surface,
            'gps_surface'           =>  $this->gps_surface,
            'harvested_surface'     =>  $this->harvested_surface,
            'abandoned_surface'     =>  $this->abandoned_surface,
            'cleared_surface'       =>  $this->cleared_surface,
            'stricken_surface'      =>  $this->stricken_surface,
            'zone_id'               => $this->zone_id,
            'campaign_id'           => $this->campaign_id,
            'contract'              => $this->contract_id,
            'third_party_id'        =>  $this->third_party_id,
            'third_party_code'      =>  ag_code($this->third_party_id),
            
            'application_date'      =>  $this->application_date,
            'expiration_date'       =>  $this->expiration_date,
            'signature_date'        =>  $this->signature_date,
            'contract_type'         =>  enum('agreement.contract_types', $this->contract_type),
            'status'                =>  enum('agreement.contract_status' , $this->status),
            'contracted_surface'    =>  $this->contracted_surface,
            'compaign_surface'      =>  $this->compaign_surface,
            'campaigns_name'        =>  date('Y', strtotime($this->campaigns_start_date)).'/'.date('Y', strtotime('+ 1 year', strtotime($this->campaigns_start_date))),
            'third_party_id'        =>  $this->third_party_id,
            'third_party_code'      =>  ag_code($this->third_party_id),
            'code_as400'            =>  $this->code_as400,
            'third_party_type'      =>  enum('thirdParty.third_party_types' ,$this->third_party_type),
            'company_name'          =>  $this->company_name,
            'rc'                    =>  $this->rc,
            'patent_number'         =>  $this->patent_number,
            'if'                    =>  $this->if,
            'ice'                   =>  $this->ice,
            'cin'                   =>  $this->cin,
            'full_name'             =>  $this->full_name,
            'soil_id'               =>  $this->soil_id,
            'registration_number'   =>  $this->registration_number,
            'rural_commune'         =>  $this->rural_commune,
            'perimeter'             =>  $this->perimeter,
            'region'                =>  $this->region,
            'district'              =>  $this->district,
            'cda'                   =>  $this->cda,
            'zone'                  =>  $this->zone,
            'sector'                =>  $this->sector,
            'block'                 =>  $this->block,
            'total_surface'         =>  $this->total_surface,
            'bare_surface'          =>  $this->bare_surface,
            'identified_surface'    =>  $this->identified_surface,
            'soil_number'           =>  $this->soil_number,
            'culture_number'        =>  $this->culture_number,
            'legal_status'          =>  $this->legal_status,
        ];
    }
}
