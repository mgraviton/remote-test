<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */
 
namespace SIAM618\Agreement\Http\Resources\LogicalParcel;

use Illuminate\Http\Resources\Json\JsonResource;

class LogicalParcelsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    =>  $this->id,
            'name'                  =>  $this->name,
            'physical_parcels'      =>  LogicalParcelResource::collection($this->collection)
        ];
    }
}
