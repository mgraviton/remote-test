<?php

namespace SIAM618\Agreement\Http\Resources\LogicalParcel;
use SIAM618\Agreement\Http\Resources\ParcelIdentifierResource;
use Illuminate\Http\Resources\Json\JsonResource;

class LogicalParcelIdentifierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'name' => $this->name,
            'annuel_surface' => $this->annuel_surface,
            'exploited_surface' => $this->exploited_surface,
            'manuel_surface' => $this->manuel_surface,
            'gps_surface' => $this->gps_surface,
            'harvested_surface' => $this->harvested_surface,
            'abandoned_surface' => $this->abandoned_surface,
            'cleared_surface' => $this->cleared_surface,
            'stricken_surface' => $this->stricken_surface,
            'parcels' => ParcelIdentifierResource::collection($this->parcels)
        ];
    }
}
