<?php

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use SIAM618\Base\Database\Models\Zone;

class SoilResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'registration_number' => $this->registration_number,
            'rural_commune' => $this->rural_commune,
            'perimeter' => $this->perimeter,
            'region' => $this->region,
            'district' => $this->district,
            'cda' => Zone::find($this->cda)['name'],
            'cda_code' => Zone::find($this->cda)['code'],
            'cda_id' => Zone::find($this->cda)['id'],
            'zone' => Zone::find($this->zone)['name'],
            'zone_code' => Zone::find($this->zone)['code'],
            'zone_id' => Zone::find($this->zone)['id'],
            'sector' => $this->sector,
            'block' => $this->block,
            'total_surface' => $this->total_surface,
            'bare_surface' => $this->bare_surface,
            'identified_surface' => $this->identified_surface,
            'soil_number' => $this->soil_number,
            'culture_number' => $this->culture_number,
            'legal_status' => $this->legal_status,
            'coordinate' => $this->coordinate,
            ];
    }
}
