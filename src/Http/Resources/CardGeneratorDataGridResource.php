<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CardGeneratorDataGridResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->cardId,
            'soilId' => (string) $this->soilId,
            'status' => $this->status,
            'perimeter' => $this->perimeter,
            'region' => $this->region,
            'tenure' => $this->tenure,
            'cda' => $this->cda,
            'zone' => $this->zone,
            'name' => $this->name,
            'name_ar' => $this->name_ar,
            'address' => $this->address,
            'tel' => $this->tel,
            'structure_id' => $this->structure_id,
          
            // 'sector' => $this->sector,
            'block' => $this->block,
            'cin' => $this->cin,
            'full_name' => $this->full_name,
            'full_name_ar' => $this->full_name_ar,
            'code' => $this->code,
            'company_name' => $this->company_name,
            'ice' => $this->ice,
        ];
    }
}
