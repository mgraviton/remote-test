<?php

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use SIAM618\Base\Database\Models\Zone;

class SoilIdentifierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'registration_number' => $this->registration_number,
            'rural_commune' => $this->rural_commune,
            'perimeter' => $this->perimeter,
            'region' => $this->region,
            'district' => $this->district,
            'cda' => Zone::find($this->cda)['name'],
            'cda_id' => $this->cda,
            'cda_code' => Zone::find($this->cda)['code'],
            'zone' => Zone::find($this->zone)['name'],
            'zone_id' => $this->zone,
            'zone_code' => Zone::find($this->zone)['code'],
            'sector' => $this->sector,
            'block' => $this->block,
            'total_surface' => $this->total_surface,
            'bare_surface' => $this->bare_surface,
            'identified_surface' => $this->identified_surface,
            'soil_number' => $this->soil_number,
            'culture_number' => $this->culture_number,
            'legal_status' => $this->legal_status,
            ];
    }
}
