<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CardDataGridResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->cardId,
            'soilId' => (string) $this->soilId,
            'Cardstatus' => enum('agreement.card_status',$this->Cardstatus),
            'perimeter' => $this->perimeter,
            'region' => $this->region,
            'district' => $this->district,
            'cda' => $this->cda,
            'zone' => $this->zone,
            'sector' => $this->sector,
            'block' => $this->block,
            'cin' => $this->cin,
            'structure_name' => $this->structure_name,
            'full_name' => $this->full_name,
            'code' => $this->code,
            'tel1' => $this->tel1,
            'birth_date' => $this->birth_date,
            'address' => $this->address,
            'advisers' => $this->advisers,
            'rib' => $this->bank_code . $this->bank_account_number . $this->bank_rib_key,
            'rfid' => $this->rfid,
            'printed_at' => $this->printed_at,
            'company_name' => $this->company_name,
            'ice' => $this->ice,
        ];
    }
}
