<?php

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use SIAM618\Base\Http\Resources\Document\DocumentTypeIdentifierResource;

class DocumentIdentifierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'user_libelle' => $this->user_libelle,
            'sys_libelle' => $this->sys_libelle,
            'path' => $this->path,
            'exention' => $this->exention,
            'size' => $this->size,
            'type' => $this->type
        ];
    }
}
