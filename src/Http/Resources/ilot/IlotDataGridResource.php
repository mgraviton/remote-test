<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Http\Resources\ilot;

use Illuminate\Http\Resources\Json\JsonResource;

class IlotDataGridResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'parcel_id' => $this->parcel_id,
            'cda_id' => $this->cda_id,
            'zone_id' => $this->zone_id,
            'ilot_id' => $this->ilot_id,
            'polygon' => $this->polygon,
            'imei' => $this->imei,
            'supplier_id' => $this->supplier_id,
            'gps_box_name' => $this->gps_box_name,
            'user_id' => $this->user_id,
            'date_semis' => $this->date_semis,
            'deleted_at' => $this->deleted_at,
            'ste_id' => $this->ste_id
        ];
    }
}
