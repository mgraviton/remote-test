<?php

namespace SIAM618\Agreement\Http\Resources\lists\contracts;

use Illuminate\Http\Resources\Json\JsonResource;
use SIAM618\Base\Database\Models\Zone;

class ContractsListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code_contrat' => $this->code_contrat,
            'cin' => $this->cin,
            'nom_complet' => $this->nom_complet,
            'date_application' => $this->date_application,
            'date_expiration' => $this->date_expiration,
            'type' => $this->type,
            'statut' => $this->statut,
            'division' => $this->division,
            'structure_id' => $this->structure_id,
            'zone_id' => $this->zone_id
        ];
    }
}
