<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SoilDataGridResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (String) $this->id,
            'registration_number'   => $this->registration_number,
            'rural_commune'         => $this->rural_commune,
            'perimeter'             => $this->perimeter, 
            'region'                => $this->region, 
            'district'              => $this->district, 
            'cda'                   => $this->cda, 
            'zone'                  => $this->zone, 
            'sector'                => $this->sector, 
            'block'                 => $this->block, 
            'total_surface'         => $this->total_surface,
            'bare_surface'          => $this->bare_surface,
            'identified_surface'    => $this->identified_surface,
            'soil_number'           => $this->soil_number,
            'culture_number'        => $this->culture_number,
            'legal_status'          => $this->legal_status,
            'coordinate'            => $this->coordinate,
        ];
    }
}