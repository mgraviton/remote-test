<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Http\Resources\todos;

use Illuminate\Http\Resources\Json\JsonResource;

class TodosDataGridResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'parcel_id'  => $this->parcel_id,
            'cda_id' => $this->cda_id,
            'zone_id' => $this->zone_id,
            'date_debut' => $this->date_debut,
            'date_fin' => $this->date_fin,
            'user_id' => $this->user_id,
            'type' => $this->type,
            'cda_name' => $this->cda_name,
            'zone_name' => $this->zone_name,
            'parcelle_name' => $this->parcelle_name,
            'third_party_full_name' => $this->third_party_full_name,
            'third_party_cin' => $this->third_party_cin,
            'third_party_code' => $this->third_party_code,
            'advisors' => $this->advisors
        ];
    }
}
