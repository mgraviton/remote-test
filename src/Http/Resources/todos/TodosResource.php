<?php

namespace SIAM618\Agreement\Http\Resources\todos;

use Illuminate\Http\Resources\Json\JsonResource;

class TodosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->id,
            'parcel_id'  => $this->parcel_id,
           'cda_id' => $this->cda_id,
           'zone_id' => $this->zone_id,
           'date_debut' => $this->date_debut,
           'date_fin' => $this->date_fin,
           'user_id' => $this->user_id,
           'type' => $this->type
        ];
    }
}
