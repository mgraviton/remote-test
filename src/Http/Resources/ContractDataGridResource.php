<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018.
 */

namespace SIAM618\Agreement\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContractDataGridResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (string) $this->contract_id,
            'parent_id' => $this->parent_contract_id,
            'contract_type' => $this->parent_contract_id ? 'Avenant' : 'Contrat',
            'code' => $this->code,
            'application_date' => $this->application_date,
            'expiration_date' => $this->expiration_date,
            'signature_date' => $this->signature_date,
            'status' => $this->status,
            'structure_name' => $this->structure_name,
            'structure_code' => $this->structure_code,
            'structure_type' => $this->structure_type,
            'third_code' => $this->third_party_code,
            'third_type' => $this->type,
            'third_company_name' => $this->company_name,
            'third_rc' => $this->rc,
            'third_patent_number' => $this->patent_number,
            'third_if' => $this->if,
            'third_ice' => $this->ice,
            'third_cin' => $this->cin,
            'third_full_name' => $this->full_name
        ];
    }
}
