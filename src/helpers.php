<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

if (!function_exists('agreement_function')) {
    /**
     * Description
     */
    function agreement_function()
    {
    }
}

/*
if (!function_exists('contract_code')) {
    function contract_code($application_date, $id)
    {
        return date('Y', strtotime($application_date)) . '/' . sprintf('%06d', $id);
    }
}
*/
if (!function_exists('contract_code_chargement')) {

    function contract_code_chargement($application_date,$division_id, $code = null)
    {
        if ($code) {
            $d = explode('/', $code);
            $d['0'] = 'AV/' . $d['0'];
            return implode('/', $d);
        }
        $annee = date('Y', strtotime($application_date));
        $site = site_code($division_id);
        $code = sprintf('%06d', contract_count($division_id));
        return  "$annee/$site/$code";
    }
}

if (!function_exists('contract_code')) {

    function contract_code($contract)
    {
        if ($contract->parent_id) {
            $d = explode('/', $contract->code);
            $d['0'] = 'AV' . $d['0'];
            return implode('/', $d);
        }
        $annee = date('Y', strtotime($contract->application_date));
        $site = site_code(user_division_id());
        $code = sprintf('%06d', contract_count(user_division_id()));
        return  "$annee/$site/$code";
    }
}

if (!function_exists('contract_count')) {
    function contract_count($division_id) {
        return \DB::table('contracts')->where('structure_id', $division_id)->count() + 1;
    }
}

if (!function_exists('site_code')) {
    function site_code($division_id) {
      if ($division_id == 10) {
        return "11";
      } else if ($division_id == 11) {
        return "12";
      } else if ($division_id == 5) {
        return "21";
      } else if ($division_id == 7) {
        return "31";
      } else if ($division_id == 6) {
        return "32";
      } else if ($division_id == 9) {
        return "41";
      } else if ($division_id == 8) {
        return "42";
      }
      return "11";
    }
}

if (!function_exists('currentCampaign')) {
    /**
     * Description
     */
    function currentCampaign($structure_id)
    {
        $campaign = \SIAM618\Base\Database\Models\Campaign::where('structure_id', $structure_id)
            ->whereNull('end_date')->firstOrFail();
        return $campaign->id;
    }
}
