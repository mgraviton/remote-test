<?php
/**
 *  Copyright (C) Graviton, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ismail RBOUH <i.rbouh@graviton.ma>, May 2018
 *
 */

return [
    'contract_types' => [
        'multiyear' => 'Pluriannuel',
        'annual' => 'Annuel'
    ],
    'contract_status' => [
        'draft' => 'Brouillon',
        'inprogress' => 'Encours',
        'actif' => 'Actif',
        'inactif' => 'Inactif',
        'suspended' => 'Suspendu',
        'blocked' => 'Bloqué',
        'expired' => 'Expiré'
    ],
    'status_eng' => [
        'Brouillon' => 'draft',
        'Encours' => 'inprogress',
        'Actif' => 'actif',
        'Inactif' => 'inactif',
        'Suspendu' => 'suspended',
        'Bloqué' => 'blocked',
        'Expiré' => 'expired'
    ],
    'irrigation_mode' => [
        'gravity' => 'Gravitaire',
        'sprinkling' => 'Aspersion'
    ],
    'card_status' => [
        'active' => 'active',
        'inactive' => 'inactive',
        'lost' => 'Perdue',
        'inproduction' => 'En fabrication'
    ],
    'tenure' => [
        'MA' => 'MELK - ASSOCIE',
        'JO' => 'JOUMOUE',
        'LO' => 'LOCATION',
        'LM' => 'LOCATION - MELK',
        'LA' => 'LOCATION - ASSOCIE',
        'PR' => 'PROCURATION',
        'AS' => 'ASSOCIE',
        'CO' => 'COOPERATIVE',
        'ME' => 'MELK',
        'MP' => 'MELK - PROCURATION',
        'LP' => 'LOCATION - PROCURATION',
        'HA' => 'HABOUS',
        'DL' => 'DOMANIALE - LOCATION ',
    ]
];